//
//  SDCustomAnimaLib.h
//  SweetDream
//
//  Created by desmond on 13-3-20.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface SDCustomAnimaLib : NSObject

@property CCSpriteBatchNode *faceSpriteSheet;

+(SDCustomAnimaLib* ) getCustomAnimaLibManager;

+(CCSequence *)eyeBrownAnimaRight;

+(CCSequence *)eyeBrownAnimaLeft;

+(CCSequence *)headAnima;

-(CCSpriteBatchNode *)SpriteSheetWithGender:(int) gender;

//animation
-(CCAnimate *)mouthAnima1 :(int) gender;

-(CCAnimate *)mouthAnima2 :(int) gender;

-(CCAnimate *)mouthAnima3 :(int) gender;

-(CCAnimate *)blinkEyeAnima :(int) gender;

-(CCAnimate *)lookAroundAnima :(int) gender;

@end
