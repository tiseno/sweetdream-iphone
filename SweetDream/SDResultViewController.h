//
//  SDResultViewController.h
//  SweetDream
//
//  Created by desmond on 13-3-7.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "SDCustomNavigationItem.h"
#import "SDAppDelegate.h"
#import "SDUserData.h"
#import "SDSimpleAnimation.h"
#import "cocos2d.h"
#import "MPFoldEnumerations.h"
#import "MPFlipEnumerations.h"
#import "MPFoldTransition.h"
#import "MPFlipTransition.h"
#import "RecommendedPillowView.h"
#import "NMCustomLabel.h"
#import "NMCustomLabelStyle.h"
#import "SDAppDelegate.h"
#import "SDFAQTable.h"
//#import "Facebook.h"


@interface SDResultViewController : UIViewController<SDFAQTableDelegate,UIActionSheetDelegate,MFMailComposeViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UILabel *supportFactorLabel;
@property (strong, nonatomic) IBOutlet UILabel *supportFactorResultLabel2;
@property (strong, nonatomic) IBOutlet UILabel *supportFactorResultLabel;
@property (strong, nonatomic) IBOutlet UILabel *descLabel;
@property (strong, nonatomic) IBOutlet UILabel *orOboveLabel;
@property (strong, nonatomic) IBOutlet UILabel *slideUPLabel;
@property (strong, nonatomic) IBOutlet UIView *resultView;
@property (strong, nonatomic) IBOutlet UILabel *slideUpTitleLabel;
@property (strong, nonatomic) IBOutlet SDCustomNavigationItem *navItem;
@property (strong, nonatomic) IBOutlet UIImageView *resultArrow;
@property (strong, nonatomic) IBOutlet UIButton *playButton;
@property (strong, nonatomic) NSString *recommendedPillow;
@property (strong, nonatomic) IBOutlet UIView *resultContentView;
@property (strong, nonatomic) NSMutableArray *viewPages;
@property (strong, nonatomic) IBOutlet UIView *resultDropdownView;


- (IBAction)backToIndex:(id)sender;
@property (nonatomic) int pageindex;
@property (nonatomic) int pageNumber;      //start from page 2
@property (strong, nonatomic) IBOutlet UIButton *indexButton;

- (IBAction)shareLink:(id)sender;

- (IBAction)goSweetDreamWebsite:(id)sender;
- (IBAction)flipPage:(id)sender;


- (IBAction)swipPage:(UISwipeGestureRecognizer *)recognizer;

- (IBAction)playVideo:(id)sender;
- (IBAction)showHideResult:(id)sender;
@end
