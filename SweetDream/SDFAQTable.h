//
//  SDFAQTable.h
//  SweetDream
//
//  Created by desmond on 13-4-25.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SDFAQTableDelegate<NSObject>

-(void)didSelected:(NSIndexPath *)indexPath;

@end

@interface SDFAQTable : UITableView<UITableViewDataSource,UITableViewDelegate>
@property(retain,nonatomic) NSArray* data;
@property(retain, nonatomic) id<SDFAQTableDelegate> FAQdelegate;
- (id)initWithFrame:(CGRect)frame AndDataArray:(NSArray *)data;

@end
