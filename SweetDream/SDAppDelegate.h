//
//  SDAppDelegate.h
//  SweetDream
//
//  Created by desmond on 13-3-4.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClockView.h"
#import "cocos2d.h"
#import <AVFoundation/AVAudioPlayer.h>
#import "SoundEffect.h"
#import <FacebookSDK/FacebookSDK.h>

@interface SDAppDelegate : UIResponder <UIApplicationDelegate>{
    ClockView *clockView;
	CCDirectorIOS	*__unsafe_unretained director_;							// weak ref
}

@property (nonatomic, strong) UIWindow *window;
@property (strong, nonatomic) FBSession *session;
@property (unsafe_unretained, readonly) CCDirectorIOS *director;
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;

+ (CGFloat) window_height;
+ (CGFloat) window_width;
+ (void)playSoundEffect:(int) track;
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;
-(void) postOnFB;
@end
