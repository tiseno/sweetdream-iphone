//
//  ColorBarCode.m
//  SweetDream
//
//  Created by desmond on 13-4-2.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import "ColorBarCode.h"

@implementation ColorBarCode
@synthesize green,green2;
@synthesize yellow,yellow2;
@synthesize orange,orange2;
@synthesize red,red2;

static ColorBarCode *colorCode = nil;

-(id)init{
    self = [super init];
    if(self){
        green = ccc3(255, 255, 109);
        green2 = ccc3(172, 215, 0);
        yellow = ccc3(252, 238, 119);
        yellow2 = ccc3(255, 192, 0);
        orange = ccc3(255, 136, 84);
        orange2 = ccc3(254, 78, 0);
        red = ccc3(250, 106, 43);
        red2 = ccc3(232, 27, 0);
        return self;
    }
    
    return  nil;
}

+ (ColorBarCode *)sharedColorBarCode{
    
    @synchronized([ColorBarCode class]) {
        if (!colorCode)
            [[self alloc] init];
        
        return colorCode;
    }
    
    return nil;
}

+ (id)alloc {
    @synchronized([ColorBarCode class]) {
        
        NSAssert(colorCode == nil, @"UserData already alloc");
        colorCode = [super alloc];
        
        return colorCode;
    }
    return nil;
}

@end
