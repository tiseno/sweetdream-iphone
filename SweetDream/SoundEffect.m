//
//  SoundEffect.m
//  SweetDream
//
//  Created by desmond on 13-3-27.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import "SoundEffect.h"

@implementation SoundEffect

- (id)initWithSoundNamed:(NSString *)filename
{
    if ((self = [super init]))
    {
        NSURL *fileURL = [[NSBundle mainBundle] URLForResource:filename withExtension:@"mp3"];
        NSURL *fileURL2 = [[NSBundle mainBundle] URLForResource:@"tick" withExtension:@"mp3"];
        if (fileURL != nil)
        {
            SystemSoundID theSoundID;
            OSStatus error = AudioServicesCreateSystemSoundID((__bridge CFURLRef)fileURL, &theSoundID);
            
            //            NSLog(@"soundID %d, error %@",soundID,error);
            if (error == kAudioServicesNoError)
                soundID = theSoundID;
        }
        
        if (fileURL2 != nil)
        {
            SystemSoundID theSoundID2;
            OSStatus error2 = AudioServicesCreateSystemSoundID((__bridge CFURLRef)fileURL2, &theSoundID2);
            
            //            NSLog(@"soundID %d, error %@",soundID,error);
            if (error2 == kAudioServicesNoError)
                soundID2 = theSoundID2;
        }
    }
    return self;
}

- (void)dealloc
{
    AudioServicesDisposeSystemSoundID(soundID);
    AudioServicesDisposeSystemSoundID(soundID2);
}

- (void)play
{
    AudioServicesPlaySystemSound(soundID);
}

- (void)playTick{
    AudioServicesPlaySystemSound(soundID2);
}
@end