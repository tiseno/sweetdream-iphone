//
//  SDBmiResultViewController.h
//  SweetDream
//
//  Created by desmond on 13-4-4.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDSimpleAnimation.h"
#import "SDUserData.h"
#import "SDCustomNavigationItem.h"

@interface SDBmiResultViewController : UIViewController<SimpleAnimationDelegate>
@property (strong, nonatomic) IBOutlet UILabel *bmiLabel;
@property (strong, nonatomic) IBOutlet UILabel *descLabel;
@property (strong, nonatomic) IBOutlet UIView *resultDropdownView;
@property (strong, nonatomic) IBOutlet UIImageView *bmiImage;
@property (strong, nonatomic) IBOutlet SDCustomNavigationItem *navItem;

- (IBAction)playVideo:(id)sender;

@end
