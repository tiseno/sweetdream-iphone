//
//  SDSleepingPositionViewController.m
//  SweetDream
//
//  Created by desmond on 13-3-5.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import "SDSleepingPositionViewController.h"

@interface SDSleepingPositionViewController ()

@end

@implementation SDSleepingPositionViewController
@synthesize navItem, scrollView, hintLabel;
@synthesize arrow;
@synthesize titleLabel,title_background;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    @autoreleasepool {
        //refer to http://refactr.com/blog/2012/09/ios-tips-custom-fonts/
        [hintLabel setFont:[UIFont fontWithName:@"CreteRound-Regular" size:hintLabel.font.pointSize]];
        [self.navItem init];
        [self.navItem.leftButton addTarget:self action:@selector(handlePrev:) forControlEvents:UIControlEventTouchUpInside];
        self.navItem.rightBarButtonItem.enabled =NO;

        arrow = [[SDPointerArrowView alloc]init];
        [self.view addSubview:arrow];
        [arrow hideLeftArrow:YES];
        [self initScrollView];
        
        [super viewDidLoad];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if([SDUserData sharedSDUserData].sleppingPosition != -1){
        self.navItem.rightBarButtonItem.enabled = YES;
        [self.navItem.rightButton addTarget:self action:@selector(handleNext:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    //bottom title animation
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height - 44;
    [[SDSimpleAnimation getAnimationManager] doAnimation:titleLabel startPoint:CGPointMake(20, screenHeight) maxPoint:CGPointMake(20, screenHeight - 82) endPoint:CGPointMake(20, screenHeight - 79) AndDirectionIsUP:YES];
    
    [[SDSimpleAnimation getAnimationManager] doAnimation:title_background startPoint:CGPointMake(2, screenHeight) maxPoint:CGPointMake(2, 439) endPoint:CGPointMake(2, 441) AndDirectionIsUP:YES];
}

-(void)initScrollView{
    NSString *gender = ([SDUserData sharedSDUserData].gender == 0)? @"male_": @"female_";
    NSMutableArray *characters = [[NSMutableArray alloc] init];
    [characters addObject:[[UIImageView alloc]
                           initWithImage:
                           [UIImage imageNamed:
                            [[NSString alloc]initWithFormat:@"%@backsleeper" , gender]
                            ]
                           ]];
    
    [characters addObject:[[UIImageView alloc]
                           initWithImage:
                           [UIImage imageNamed:
                            [[NSString alloc]initWithFormat:@"%@sidesleeper" , gender]
                            ]
                           ]];
    
    [characters addObject:[[UIImageView alloc]
                           initWithImage:
                           [UIImage imageNamed:
                            [[NSString alloc]initWithFormat:@"%@stomachsleeper" , gender]
                            ]
                           ]];
//    [characters addObject:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sidesleeper"]]];
//    [characters addObject:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stomachsleeper"]]];
    
    for (int i = 0; i < characters.count; i++) {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(findOutTheTag:)];
        [[characters objectAtIndex:i] addGestureRecognizer:tap];
    }

    self.scrollView = [[SDPickerScrollView alloc] initWithImageArray:characters AndScrollView:self.scrollView AndItemFrame:CGRectMake(25, 49, 270, 338)] ;
    
    [self.scrollView scrollToPage:[SDUserData sharedSDUserData].sleppingPosition];
    self.scrollView.delegate = self;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)findOutTheTag:(id)sender {
    [SDAppDelegate playSoundEffect:1]; //click sound
    [[SDSimpleAnimation getAnimationManager] doZoomInOutAnimation:((UIGestureRecognizer *)sender).view startScalePoint:CGPointMake(1.05, 1.05) EndScalePoint:CGPointMake(1.0, 1.0)];
    [SDSimpleAnimation getAnimationManager].animaDelegate = self;
    [SDSimpleAnimation getAnimationManager].delegateIdentified = 88;
    switch (((UIGestureRecognizer *)sender).view.tag)
    {
        case 0:
            [SDUserData sharedSDUserData].sleppingPosition = 0 ;
            break;
            
        case 1:
            [SDUserData sharedSDUserData].sleppingPosition = 1 ;
            break;
            
        case 2:
            [SDUserData sharedSDUserData].sleppingPosition = 2 ;
            break;
    }
}

- (void) handlePrev:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) handleNext:(id)sender
{
    [self performSegueWithIdentifier:@"inputWeightSegue" sender:self];
}

#pragma SDSimpleAnimation Delegate
-(void)didEndAnimation:(NSInteger)delegateIdentified{

    if (delegateIdentified == 88) {
        [self performSegueWithIdentifier:@"inputWeightSegue" sender:self];        
    }
}

#pragma UIScrollView Delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    float offset = scrollView.contentOffset.x;
    if(offset < 320){
        [arrow hideLeftArrow:YES];
        [arrow hideRightArrow:NO];
    }else if(offset >= scrollView.contentSize.width - 320){
        [arrow hideRightArrow:YES];
        [arrow hideLeftArrow:NO];
    }else{
        [arrow hideLeftArrow:NO];
        [arrow hideRightArrow:NO];
    }
}

- (IBAction)switchItem:(id)sender {
    
    static int scrollVIew_offset = 0;
    switch ([sender tag]) {
        case 1:
            if (!(scrollVIew_offset <= 0))
                scrollVIew_offset = scrollVIew_offset - 320;
            break;
            
        case 2:
            if (scrollVIew_offset < 640)
                scrollVIew_offset = scrollVIew_offset + 320;
            break;
    }
    [self.scrollView setContentOffset:CGPointMake(scrollVIew_offset, 0) animated:YES ];
}
@end
