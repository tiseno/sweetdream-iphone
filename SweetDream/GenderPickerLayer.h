//
//  GenderPickerLayer.h
//  SweetDream
//
//  Created by desmond on 13-3-15.
//  Copyright 2013年 desmond. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "CharacterLayer.h"
#import "SDAppDelegate.h"
#import "SDCustomLayerDelegate.h"
#import "RecommendedPillowView.h"

@interface GenderPickerLayer : CCLayer
{
}

@property (nonatomic) NSMutableArray *telephoneAreaArr;
@property (nonatomic) int selection;
@property (retain,nonatomic) NSArray *charsSet;
@property (strong,nonatomic) GenderPickerLayer *layer;

+(id) scene;
-(id) scene;

-(void)insertCharacter:(NSArray *)chars AndNeedHint:(BOOL) needHint;
-(void)removeCharacter;

-(void)registerWithTouchDispatcher;
-(void)unregisterWithTouchDispatcher;

@end
