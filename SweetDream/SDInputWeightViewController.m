//
//  SDInputWeightViewController.m
//  SweetDream
//
//  Created by desmond on 13-3-6.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import "SDInputWeightViewController.h"
#define unitTypeComponent 3
#define onesComponent 2
#define tensComponent 1
#define hundredsComponent 0

@interface SDInputWeightViewController ()

@end

@implementation SDInputWeightViewController
@synthesize hintLabel,scrollView,navItem,actionSheet,picker, clockArea;
@synthesize ones, tens, hundreds,unitType;
@synthesize genderLayer;
@synthesize titleLabel,title_background;

BOOL enteredWeight = NO; //if false then show picker

- (void)viewDidLoad
{
    @autoreleasepool {
        
        [super viewDidLoad];
        [hintLabel setFont:[UIFont fontWithName:@"CreteRound-Regular" size:hintLabel.font.pointSize]];
        [navItem init];
        navItem.rightBarButtonItem.enabled = NO;
        [navItem.leftButton addTarget:self action:@selector(handlePrev:) forControlEvents:UIControlEventTouchUpInside];
        [navItem.rightButton addTarget:self action:@selector(handleNext:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.scrollView setDelegate:self];
        [self.scrollView setContentOffset:CGPointMake(320, 0) animated:NO];
        [self.scrollView setScrollEnabled:NO];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(findOutTheTag:)];
        [self.scrollView addGestureRecognizer:tap];
        
    }

}

-(void)viewDidAppear:(BOOL)animated{
    
    genderLayer = [GenderPickerLayer alloc];
//    [CCDirector sharedDirector].view.frame = CGRectMake(0, 0, 960, 480);
    [[CCDirector sharedDirector] runWithScene:[genderLayer scene]];
    [genderLayer insertCharacter:[self getCharacter] AndNeedHint:YES];
    
    [self.scrollView addSubview:[CCDirector sharedDirector].view ];
    self.scrollView.contentSize = CGSizeMake(960, 360);
    
    [self.clockArea addSubview:[ClockView getClockView]];
    [[ClockView getClockView]start];
    [super viewDidAppear:NO];
    enteredWeight = true;
    
    if([SDUserData sharedSDUserData].weight != -1){
        self.navItem.rightBarButtonItem.enabled = YES;
        [self.navItem.rightButton addTarget:self action:@selector(handleNext:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    //bottom title animation
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height - 44;
    [[SDSimpleAnimation getAnimationManager] doAnimation:titleLabel startPoint:CGPointMake(20, screenHeight) maxPoint:CGPointMake(20, screenHeight - 72) endPoint:CGPointMake(20, screenHeight - 69) AndDirectionIsUP:YES];
    
    [[SDSimpleAnimation getAnimationManager] doAnimation:title_background startPoint:CGPointMake(2, screenHeight) maxPoint:CGPointMake(2, 439) endPoint:CGPointMake(2, 441) AndDirectionIsUP:YES];
}

-(void)viewDidDisappear:(BOOL)animated{
    [[CCDirector sharedDirector] stopAnimation];
    [[CCDirector sharedDirector].view removeFromSuperview];

}

-(NSArray*) getCharacter{
    NSString *gender = @"male_";
    
    if([SDUserData sharedSDUserData].gender == 1){
        gender = @"female_";
    }else{
        gender = @"male_";
    }
    
    CharacterLayer *chars = [CharacterLayer node];
    [chars setCharacterBody:[NSString stringWithFormat:@"%@skinny_body.png",gender]];
    [chars setCharactergender:[SDUserData sharedSDUserData].gender];
    [chars setContentSize:CGSizeMake(180, 440)];
    [chars setPosition:CGPointMake(320/2-(180/2), 44)];
    
    CharacterLayer *chars2 = [CharacterLayer node];
    [chars2 setCharacterBody:[NSString stringWithFormat:@"%@normal_body.png",gender]];
    [chars2 setCharactergender:[SDUserData sharedSDUserData].gender];
    [chars2 setContentSize:CGSizeMake(180, 440)];
    [chars2 setPosition:CGPointMake(960/2-(180/2), 44)];
    
    CharacterLayer *chars3 = [CharacterLayer node];
    [chars3 setCharacterBody:[NSString stringWithFormat:@"%@fat_body.png",gender]];
    [chars3 setCharactergender:[SDUserData sharedSDUserData].gender];
    [chars3 setContentSize:CGSizeMake(180, 440)];
    [chars3 setPosition:CGPointMake(1600/2-(180/2), 44)];
    
    return [NSArray arrayWithObjects:chars, chars2,chars3 ,nil];
}

#pragma handle when tap in image
- (void)findOutTheTag:(id)sender {
    [self showPicker];
}

#pragma Delegate UIScrollView
-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    if (enteredWeight) {
        sleep(1.5);
        [self performSegueWithIdentifier:@"inputHeightSegue" sender:self];
    }
}

#pragma Delegate CAAnimation
- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag
{
    [self showPicker];
    //    [self performSegueWithIdentifier:@"FirmnessSegue" sender:self];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) handlePrev:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) handleNext:(id)sender
{
    [self performSegueWithIdentifier:@"inputHeightSegue" sender:self];
}

- (void) showPicker{
    [[CCDirector sharedDirector] stopAnimation];
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                              delegate:nil
                                     cancelButtonTitle:nil
                                destructiveButtonTitle:nil
                                     otherButtonTitles:nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    CGRect pickerFrame = CGRectMake(0, 40, 0, 0);
    
    picker = [[UIPickerView alloc] initWithFrame:pickerFrame];
    picker.showsSelectionIndicator = YES;
    picker.dataSource = self;
    picker.delegate = self;
    [picker selectRow:ones inComponent:onesComponent animated:YES];
    [picker selectRow:tens inComponent:tensComponent animated:YES];
    [picker selectRow:hundreds inComponent:hundredsComponent animated:YES];
    [picker selectRow:0 inComponent:unitTypeComponent animated:YES];
    
    [actionSheet addSubview:picker];

    UISegmentedControl *closeButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Done"]];
    closeButton.momentary = YES;
    closeButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
    closeButton.segmentedControlStyle = UISegmentedControlStyleBar;
    closeButton.tintColor = [UIColor blackColor];
    [closeButton addTarget:self action:@selector(dismissActionSheet) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:closeButton];
    
    [actionSheet showInView:[[UIApplication sharedApplication] keyWindow]];
    
    [actionSheet setBounds:CGRectMake(0, 0, 320, 485)];
}

-(CGPoint)getScrollingOffset:(NSInteger) charWeight{
    CGPoint scrollVIew_offset = CGPointMake(0, 0);
    if (charWeight <= 55) {
        scrollVIew_offset = CGPointMake(0, 0);
        
    }else if(charWeight >= 56 && charWeight <= 75){
        scrollVIew_offset = CGPointMake(320, 0);
        
    }else if (charWeight >= 75){
        scrollVIew_offset = CGPointMake(640, 0);
    }
    return scrollVIew_offset;
}

-(void)dismissActionSheet{

    enteredWeight = true;
    //switch scrollView position
    float lbsTokgRate = unitType == 0? 1:1/2.2;
    float weight_ = (ones + tens*10 + hundreds*100)*lbsTokgRate ;
    NSLog(@"weight: %f", weight_);
    
    
    if(weight_ <= 0){
        [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
        [self showAlert];
        return;
    }
    
    [SDUserData sharedSDUserData].weight = weight_;
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    
    CGPoint scrollVIew_offset = CGPointMake(0, 0);
    if (weight_ <= 45) {
        scrollVIew_offset = CGPointMake(0, 0);
    }else if(weight_ >= 46 && weight_ <= 65){
        scrollVIew_offset = CGPointMake(320, 0);
        
    }else if (weight_ >= 66){
        scrollVIew_offset = CGPointMake(640, 0);
    }
    
    [self.scrollView setContentOffset:scrollVIew_offset animated:YES ];
    
    if(self.scrollView.contentOffset.x == scrollVIew_offset.x){
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            if (enteredWeight) {
                sleep(1.5);
                dispatch_async(dispatch_get_main_queue(),^{
                    [self performSegueWithIdentifier:@"inputHeightSegue" sender:self];
                });
            }
        });
        
        
    }

}

-(void)showAlert{
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Weight Error" message:@"Please enter your weight" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];

}

#pragma mark-
#pragma mark- UIAlertView delegate
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    [self performSelector:@selector(showPicker) withObject:nil afterDelay:0.4];

}

#pragma mark-
#pragma mark Picker Data Source Methods
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 4;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    switch (component) {
        case hundredsComponent:
            return 5;
            break;
            
        case tensComponent:
            return 10;
            break;
            
        case onesComponent:
            return 10;
            break;
            
        case unitTypeComponent:
            return 1;
            break;
    }
    
    return 10;
}

#pragma mark Picker Delegate Methods
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSArray *unitType = [[NSArray alloc] initWithObjects:@"kg",@"lbs",nil];
    if(component != unitTypeComponent )
        return [[NSString alloc] initWithFormat:@"%d",row];
    else{
        return [[NSString alloc] initWithFormat:@"%@",[unitType objectAtIndex:row]];
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    switch (component) {
        case hundredsComponent:
            hundreds = row;
            break;
            
        case tensComponent:
            tens = row;
            break;
            
        case onesComponent:
            ones = row;
            break;
            
        case unitTypeComponent:
            unitType = row;
            break;
    }
    
}

-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    switch (component) {
        case hundredsComponent:
            return 80;
            break;
            
        case tensComponent:
            return 80;
            break;
            
        case onesComponent:
            return 80;
            break;
            
        case unitTypeComponent:
            return 60;
            break;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self setGenderLayer:nil];
    [[CCDirector sharedDirector] stopAnimation];
    [[CCDirector sharedDirector] popScene];
    [[CCDirector sharedDirector].view removeFromSuperview];
    
}

- (void)viewDidUnload {
    [self setClockArea:nil];
    [self setTitleLabel:nil];
    [super viewDidUnload];
}
@end
