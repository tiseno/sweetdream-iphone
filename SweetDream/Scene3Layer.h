//
//  Scene3Layer.h
//  SweetDream
//
//  Created by desmond on 13-4-3.
//  Copyright 2013年 desmond. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Scene4Layer.h"
#import "SDUserData.h"

@interface Scene3Layer : CCLayer {
    
}
@property(retain,nonatomic) CCSprite *charactor;
@property(retain,nonatomic) CCSequence *sc_acs;
+(CCScene *) scene;

@end
