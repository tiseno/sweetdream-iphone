//
//  SDBmiResultViewController.m
//  SweetDream
//
//  Created by desmond on 13-4-4.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import "SDBmiResultViewController.h"

@interface SDBmiResultViewController ()

@end

@implementation SDBmiResultViewController
@synthesize resultDropdownView,bmiLabel,descLabel,bmiImage,navItem;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [bmiLabel setFont:[UIFont fontWithName:@"CreteRound-Regular" size:bmiLabel.font.pointSize]];
    [descLabel setFont:[UIFont fontWithName:@"CreteRound-Regular" size:descLabel.font.pointSize]];
    self.navItem.hidesBackButton = YES;
    
    [self doCalculation];
}


-(void)viewWillAppear:(BOOL)animated{
    [[SDSimpleAnimation getAnimationManager] doAnimation:resultDropdownView startPoint:CGPointMake(7, -175) maxPoint:CGPointMake(7, 0) endPoint:CGPointMake(7, -11) AndDirectionIsUP:NO];
}

-(void)viewWillDisappear:(BOOL)animated{
     [SDSimpleAnimation getAnimationManager].animaDelegate = nil;
}

-(void)doCalculation{
    float weight = [SDUserData sharedSDUserData].weight;
    float height = [SDUserData sharedSDUserData].height / 100.0;
    height = (height == 0) ? 1 : height;
    float bmi = weight/(height*height);
    [SDUserData sharedSDUserData].bmi = (isnan(bmi)) ? 0 : bmi ;

    bmiLabel.text = [NSString stringWithFormat:@"%.1f",[SDUserData sharedSDUserData].bmi];
    
    //bmi
    NSRange under = NSMakeRange(0 * 100, 19.0 * 100);
    
    NSRange normal1 = NSMakeRange(19.00 * 100, (20.50 - 19.00)* 100);
    NSRange normal2 = NSMakeRange(20.50 * 100, (22.50 - 20.50)* 100);
    NSRange normal3 = NSMakeRange(22.50 * 100, (25.00 - 22.50)* 100);
    
    NSRange over1 = NSMakeRange(25.00 * 100, (27.50 - 25.00)* 100);
    NSRange over2 = NSMakeRange(27.50 * 100, (30.00 - 27.50)* 100);
    
    NSRange obese1 = NSMakeRange(30.00 * 100, (35.00 - 30.00) * 100);
    NSRange obese2 = NSMakeRange(35.00 * 100, (40.00 - 35.00) * 100);
    NSRange obese3 = NSMakeRange(40.00 * 100, (4999900 * 100));
    
    NSMutableDictionary *bmiRangeDict = [[NSMutableDictionary alloc] init];
    [bmiRangeDict setObject:[NSValue valueWithRange:under] forKey:@"under"];
    [bmiRangeDict setObject:[NSValue valueWithRange:normal1] forKey:@"normal1"];
    [bmiRangeDict setObject:[NSValue valueWithRange:normal2] forKey:@"normal2"];
    [bmiRangeDict setObject:[NSValue valueWithRange:normal3] forKey:@"normal3"];
    [bmiRangeDict setObject:[NSValue valueWithRange:over1] forKey:@"over1"];
    [bmiRangeDict setObject:[NSValue valueWithRange:over2] forKey:@"over2"];
    [bmiRangeDict setObject:[NSValue valueWithRange:obese1] forKey:@"obese1"];
    [bmiRangeDict setObject:[NSValue valueWithRange:obese2] forKey:@"obese2"];
    [bmiRangeDict setObject:[NSValue valueWithRange:obese3] forKey:@"obese3"];
    
    //read all branch and state from plist
    NSBundle *bundle = [NSBundle mainBundle];
    NSURL *plistURL = [bundle URLForResource:@"bmiTable" withExtension:@"plist"];
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfURL:plistURL];
   
    for(id key in bmiRangeDict){
        NSRange range = [[bmiRangeDict objectForKey:key] rangeValue];
        if(NSLocationInRange(bmi * 100,range)){
            NSArray *sleepingPosWithResult = [dictionary objectForKey:key];
            NSArray *resultData = [sleepingPosWithResult objectAtIndex:[SDUserData sharedSDUserData].sleppingPosition];
            NSString *supportFactor;
            
            NSString *sleppingPositionString = @"";
            switch ([SDUserData sharedSDUserData].sleppingPosition) {
                case 0:
                    sleppingPositionString = @"Supine Posture (side sleeper)";
                    break;
                    
                case 1:
                    sleppingPositionString = @"Lateral Posture (back sleeper)";
                    break;
                    
                case 2:
                    sleppingPositionString = @"Prone Posture (stomach sleeper)";
                    break;
            }
            
            [SDUserData sharedSDUserData].recommendedPillow = [resultData objectAtIndex:1];
            
            [SDUserData sharedSDUserData].supportFactorValue = [[NSString alloc] initWithFormat:@"%@",[resultData objectAtIndex:0]];
            NSLog(@"bmi %f",bmi);
            NSLog(@"key %@",key);
            NSLog(@"supportFactorValue %@",[SDUserData sharedSDUserData].supportFactorValue );
            NSLog(@"recommendedPillow %@",[SDUserData sharedSDUserData].recommendedPillow );
            NSMutableString *fileName = [[NSMutableString alloc]initWithFormat:@""];
            if([SDUserData sharedSDUserData].gender == 1){
                [fileName appendString:@"female_"];
            }else{
                [fileName appendString:@"male_"];
            }
            
            if([key isEqualToString:@"under"]){
                [fileName appendString:@"bmi_underweight"];
                [SDUserData sharedSDUserData].supportFactor = @"under";
                supportFactor = @"Under Weight";
            }else if([key isEqualToString:@"normal1"] || [key isEqualToString:@"normal2"] || [key isEqualToString:@"normal3"]){
                [fileName appendString:@"bmi_normal"];
                [SDUserData sharedSDUserData].supportFactor = @"normal";
                supportFactor = @"Normal";
            }else if([key isEqualToString:@"over1"] || [key isEqualToString:@"over2"] ){
                [fileName appendString:@"bmi_overweight"];
                [SDUserData sharedSDUserData].supportFactor = @"over";
                supportFactor = @"Over Weight";
            }else if([key isEqualToString:@"obese1"] || [key isEqualToString:@"obese1"] || [key isEqualToString:@"obese3"]){
                [fileName appendString:@"bmi_obese"];
                [SDUserData sharedSDUserData].supportFactor = @"obese";
                supportFactor = @"Obese";
            }
            bmiImage.image = [UIImage imageNamed:fileName];
            [SDUserData sharedSDUserData].supportFactorNsleepPosture = [NSString stringWithFormat:@"You are %@ \n %@",supportFactor,sleppingPositionString];
        }
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}

-(void)didEndAnimation:(NSInteger)delegateIdentified{
    if (delegateIdentified == 99) {
        [self performSegueWithIdentifier:@"VideoSegue" sender:self];
    }

}

- (void)viewDidUnload {
    [self setBmiLabel:nil];
    [self setDescLabel:nil];
    [self setResultDropdownView:nil];
    [self setBmiImage:nil];
    [self setNavItem:nil];
    [super viewDidUnload];
}
- (IBAction)playVideo:(id)sender {
    [sender setEnabled:NO];
//    [SDSimpleAnimation getAnimationManager].animaDelegate = self;
//    [SDSimpleAnimation getAnimationManager].delegateIdentified = 99;
    [[SDSimpleAnimation getAnimationManager] doAnimation:resultDropdownView startPoint:CGPointMake(7, -11) maxPoint:CGPointMake(7, -400) endPoint:CGPointMake(7, -404) AndDirectionIsUP:YES];
    
    [NSTimer scheduledTimerWithTimeInterval:.5 target:self selector:@selector(goVideo) userInfo:nil repeats:NO];

}

-(void)goVideo{
    [self performSegueWithIdentifier:@"VideoSegue" sender:self];
}
@end
