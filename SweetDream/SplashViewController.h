//
//  SplashViewController.h
//  iTennis
//
//  Created by Brandon Trebitowski on 3/18/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"


@interface SplashViewController : UIViewController {
	NSTimer *timer;
	UIImageView *splashImageView;
	
	SplashViewController *viewController;
}

@property(nonatomic,retain) NSTimer *timer;

@property(nonatomic,retain) SplashViewController *viewController;
@property (retain, nonatomic) IBOutlet UIImageView *splashImageView;

@end
