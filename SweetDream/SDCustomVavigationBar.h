//
//  SDCustomVavigationBar.h
//  SweetDream
//
//  Created by desmond on 13-3-4.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDCustomVavigationBar : UINavigationBar


-(void) drawRect:(CGRect)rect;
- (CGSize)sizeThatFits:(CGSize)size;

@end
