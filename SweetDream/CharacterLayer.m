//
//  CharacterLayer.m
//  SweetDream
//
//  Created by desmond on 13-3-15.
//  Copyright 2013年 desmond. All rights reserved.
//

#import "CharacterLayer.h"


@implementation CharacterLayer

@synthesize bodyLayer,eyeLayer,hairLayer,headLayer;
@synthesize body,mouth,eye, gender;
@synthesize mouthAnim1,mouthAnim2,mouthAnim3;
@synthesize characterDoPoping;

-(id)init{
    
    if((self = [super init])) {
        //testing use
//        CCLayerColor *ll = [CCLayerColor layerWithColor:ccc4(100, 0, 100, 255)];
//        [ll setPosition:ccp(self.boundingBox.origin.x, self.boundingBox.origin.y)];
//        [ll setContentSize:CGSizeMake(self.boundingBox.size.width, self.boundingBox.size.width)];
//        [self addChild:ll];
    
        
    }
    return self;
}

-(void)setCharacterBody:(NSString *)url{
    //body layer
    body = [CCSprite spriteWithFile:url];
    [body setAnchorPoint:ccp(0,0)];
    [body setPosition:ccp(0, 0)];
    
    bodyLayer = [CCLayerColor layerWithColor:ccc4(100, 0, 0, 0)];
    bodyLayer.contentSize = body.boundingBox.size;
    [bodyLayer setAnchorPoint:ccp(1.0,1.0)];
    [bodyLayer setPosition:ccp(0, 0)];
    [bodyLayer addChild:body];

}

-(void)setCharactergender:(NSInteger)gender{
    self.gender = gender;
    NSString *genderType = @"male_";
    if(self.gender == 0){
        //male
        genderType = @"male_";
    }else if(self.gender == 1){
        genderType = @"female_";
    }
    
    //CharacterLayer head
    CCSprite *head = [CCSprite spriteWithFile:[NSString stringWithFormat:@"%@head.png",genderType]];
//    CCSprite *head = [CCSprite spriteWithFile:[NSString stringWithFormat:@"female_head.png"]];
    [head setAnchorPoint:ccp(0,0)];
    [head setPosition:ccp(0,0)];
    
    headLayer = [CCLayerColor layerWithColor:ccc4(0, 0, 0, 0)];
    headLayer.contentSize = head.boundingBox.size;
    [headLayer addChild:head];
    [headLayer setAnchorPoint:ccp(0.5,0)];
    [headLayer setRotation:0];
    [headLayer setPosition:ccp((180/2) - (head.boundingBox.size.width/2), [body boundingBox].size.height -10)];
    
    CCSprite *hair = [CCSprite spriteWithFile:[NSString stringWithFormat:@"%@hair.png",genderType]];
    [hair setAnchorPoint:ccp(0,0)];
    [hair setPosition:ccp(0,0)];
    
    hairLayer = [CCLayerColor layerWithColor:ccc4(123, 0, 0, 0)];
    hairLayer.contentSize = hair.boundingBox.size;
    [hairLayer addChild:hair];
    [hairLayer setAnchorPoint:ccp(0.5,1)];
    [hairLayer setRotation:0];
    [hairLayer setPosition:ccp((180/2) - (hair.boundingBox.size.width/2), [body boundingBox].size.height - 80)];
    
    CCSprite *eyebrown_left = [CCSprite spriteWithFile:[NSString stringWithFormat:@"%@eyebrown_left.png",genderType]];
    [eyebrown_left setAnchorPoint:ccp(0.5,0.5)];
    [eyebrown_left setPosition:ccp((head.boundingBox.size.width*0.3),(head.boundingBox.size.height*.42))];
    [headLayer addChild:eyebrown_left z:0 tag:LEFT_EYEBROWM];
    
    CCSprite *eyebrown_right = [CCSprite spriteWithFile:[NSString stringWithFormat:@"%@eyebrown_right.png",genderType]];
    [eyebrown_right setAnchorPoint:ccp(0.5,0.5)];
    [eyebrown_right setPosition:ccp((head.boundingBox.size.width*0.7),(head.boundingBox.size.height*.42))];
    [headLayer addChild:eyebrown_right z:0 tag:RIGHT_EYEBROWM];
    
    
    
    
//    NSTimer *myTimer = [NSTimer scheduledTimerWithTimeInterval:((rand() % 4) + 4)
//                                                        target: self
//                                                      selector: @selector(doAnimation)
//                                                      userInfo: nil
//                                                       repeats: YES];
    
    CCSpriteBatchNode *faceSheet = [[SDCustomAnimaLib getCustomAnimaLibManager] SpriteSheetWithGender:gender];

    self.mouth = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"%@mouth_1.png",genderType]];
    [mouth setAnchorPoint:ccp(0.5,0.5)];
    [mouth setPosition:ccp((head.boundingBox.size.width/2),(head.boundingBox.size.height*.15))];
    
    
    self.eye = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"%@eye_1.png",genderType]];
    [eye setAnchorPoint:ccp(0.5,0.5)];
    [eye setPosition:ccp((head.boundingBox.size.width/2),(head.boundingBox.size.height*.3))];
    
    [faceSheet addChild:eye z:0 tag:EYE];
    [faceSheet addChild:mouth z:0 tag:MOUTH];
    
    [headLayer addChild:faceSheet];
    
    [self addChild:hairLayer z:0];
    [self addChild:bodyLayer z:1];
    [self addChild:headLayer z:2];
    [self schedule:@selector(doAnimation) interval:((rand() % 4) + 4)];
    [self doAnimation];

}

-(void)doAnimation{
    CCSprite *target = [headLayer getChildByTag:RIGHT_EYEBROWM];
    CCSprite *target2 = [headLayer getChildByTag:LEFT_EYEBROWM];
    SDCustomAnimaLib *animaLib = [SDCustomAnimaLib getCustomAnimaLibManager];
    
    if(characterDoPoping){
        [self characterPoping];
    }
    
    switch (rand() % 4) {
        case 0:
            [mouth runAction:[animaLib mouthAnima1:gender]];
            [eye runAction:[animaLib blinkEyeAnima:gender]];
            [headLayer runAction:[SDCustomAnimaLib headAnima]];
            [hairLayer runAction:[SDCustomAnimaLib headAnima]];
            break;
            
        case 1:
            [mouth runAction:[animaLib mouthAnima2:gender]];
            [eye runAction:[animaLib lookAroundAnima:gender]];
            [target  runAction:[SDCustomAnimaLib eyeBrownAnimaLeft]];
            [target2 runAction:[SDCustomAnimaLib eyeBrownAnimaRight]];
            break;
            
        case 2:
            [mouth runAction:[animaLib mouthAnima3:gender]];
            [eye runAction:[animaLib lookAroundAnima:gender]];
            [target runAction:[SDCustomAnimaLib eyeBrownAnimaLeft]];
            [headLayer runAction:[SDCustomAnimaLib headAnima]];
            [hairLayer runAction:[SDCustomAnimaLib headAnima]];
            break;
            
        case 3:
            [headLayer runAction:[SDCustomAnimaLib headAnima]];
            [hairLayer runAction:[SDCustomAnimaLib headAnima]];
            [target runAction:[SDCustomAnimaLib eyeBrownAnimaLeft]];
            [eye runAction:[animaLib blinkEyeAnima:gender]];
            [target2 runAction:[SDCustomAnimaLib eyeBrownAnimaRight]];
            break;
    }
}

-(void)characterPoping{
    
    id sc_ac0 = [CCScaleTo actionWithDuration:0.1 scale:1.02];
    id sc_ac1 = [CCScaleTo actionWithDuration:0.1 scale:1.00];
    
    CCSequence *sc_acs = [CCSequence actions:sc_ac0,sc_ac1,sc_ac0,sc_ac1, nil];
            
    [self runAction:[CCSequence actions:sc_acs,nil]];
}

-(void)setCharacterPoping:(BOOL) setPoping{
    characterDoPoping = setPoping;
//    if(setPoping)
//        [self schedule:@selector(characterPoping) interval:4];
//    else
//        [self pauseSchedulerAndActions];
}

@end
