//
//  GenderPickerLayer.m
//  SweetDream
//
//  Created by desmond on 13-3-15.
//  Copyright 2013年 desmond. All rights reserved.
//

#import "GenderPickerLayer.h"

@implementation GenderPickerLayer
@synthesize selection,layer,charsSet,telephoneAreaArr;


+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
    
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    
    GenderPickerLayer *layer = [GenderPickerLayer node];
    
    [scene addChild:layer];
    
	return scene;
}

-(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
    
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    self.layer = [GenderPickerLayer node];
    [scene addChild:self.layer];
    
	return scene;
}

-(id)init
{
    if( (self=[super init] )) {
        [self registerWithTouchDispatcher];
    }
    return self;
}

-(void)insertCharacter:(NSArray *)chars AndNeedHint:(BOOL) needHint{
    charsSet = chars;
    telephoneAreaArr = [[NSMutableArray alloc]init];
    int i = 0;
    for (CharacterLayer *charac in charsSet) {
        [charac setCharacterPoping:needHint];
        [layer addChild:charac];
        
        [telephoneAreaArr addObject:[NSValue valueWithCGRect:CGRectMake(258 + (i * 320), 302, 50, 147)]];
        i++;
    }
    

    
    [self registerWithTouchDispatcher];
}

- (void)registerWithTouchDispatcher {
    [[[CCDirector sharedDirector] touchDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

-(void)unregisterWithTouchDispatcher{
    [[[CCDirector sharedDirector] touchDispatcher] removeDelegate:self];
}

#pragma touchDispatcher
- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    CGPoint location = [layer convertTouchToNodeSpace:touch];
    selection = 0;
    
    id doneAction = [CCCallFuncN actionWithTarget:self selector:@selector(nextAction:)];
    
    id sc_ac0 = [CCScaleTo actionWithDuration:0.1 scale:1.05];
    id sc_ac1 = [CCScaleTo actionWithDuration:0.1 scale:1.00];

    
    for (id telephone in telephoneAreaArr) {
        if (CGRectContainsPoint([telephone CGRectValue], location)) {
            // 99 = telephone is tapping
            [[[SDCustomLayerDelegate getCustomLayerManager] getCustomLayerDelegate]ItemDidSelect:99];
            return YES;
        }
    }
    
    CCSequence *sc_acs = [CCSequence actions:sc_ac0,sc_ac1, nil];
    for (CharacterLayer *charac in charsSet) {
        if (CGRectContainsPoint(charac.boundingBox, location)) {
            [SDAppDelegate playSoundEffect:1];
            [charac runAction:[CCSequence actions:sc_acs,doneAction,nil]];
            break;
        }
        selection++;
    }
    return NO;
}

- (void) nextAction:(id)sender{
    [[[SDCustomLayerDelegate getCustomLayerManager] getCustomLayerDelegate]ItemDidSelect:selection];
}

- (void)onExit {
    [super onExit];
    [[CCDirector sharedDirector].touchDispatcher removeDelegate:self];
}


@end
