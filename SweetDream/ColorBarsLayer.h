//
//  ColorBarsLayer.h
//  SweetDream
//
//  Created by desmond on 13-4-2.
//  Copyright 2013年 desmond. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "ColorBar.h"

@interface ColorBarsLayer : CCLayerColor {
    
}

-(id)initWithArrayData:(NSArray*) data;
@end
