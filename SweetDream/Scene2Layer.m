//
//  Scene2Layer.m
//  SweetDream
//
//  Created by desmond on 13-4-1.
//  Copyright 2013年 desmond. All rights reserved.
//

#import "Scene2Layer.h"


@implementation Scene2Layer
@synthesize charactor;

+(CCScene *) scene
{
    @autoreleasepool {
        // 'scene' is an autorelease object.
        CCScene *scene = [CCScene node];
        
        Scene2Layer *layer = [Scene2Layer node];
        
        [scene addChild:layer];
        
        return scene;
    }
}

-(id)init{
    @autoreleasepool {
        if((self = [super init])) {
            [self initBackground];
            [self initCharactor];
            [self schedule:@selector(nextFrame:)];
            
        }
        return self;
    }
}

-(void)initCharactor{
    NSString *gender = ([SDUserData sharedSDUserData].gender == 0)? @"": @"f_";
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:[NSString stringWithFormat:
                                                                          @"%@walk.plist",gender]];
    
    CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode
                                      batchNodeWithFile:[NSString stringWithFormat:
                                                         @"%@walk.png",gender]];
    
    NSMutableArray *walkAnimFrames = [NSMutableArray array];
    for(int i =1; i <=8; ++i) {
        [walkAnimFrames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"walk_%d.png",i]]];
    }
    
    CCAnimation *walkAnim = [CCAnimation animationWithSpriteFrames:walkAnimFrames delay:0.1f];
    walkAnim.restoreOriginalFrame = YES;
    
    CGSize winSize = [CCDirector sharedDirector].winSize;
    charactor = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:
                                                     @"walk_1.png",gender]];
    charactor.position = ccp((winSize.width/2) + 100, (winSize.height/2)-10);
    CCAnimate *walkAction = [CCRepeatForever actionWithAction:
                             [CCAnimate actionWithAnimation:walkAnim]];
    [charactor runAction:walkAction];
    [spriteSheet addChild:charactor];
    
    [self addChild:spriteSheet z:1];
}

-(void)initBackground{
    CGSize winSize = [CCDirector sharedDirector].winSize;
    CCSprite *background = [CCSprite spriteWithFile:@"bg_scene_2.png"];
    [background setAnchorPoint:ccp(0.5,0.5)];
    [background setPosition:ccp(winSize.width /2,winSize.height/2)];
    [self addChild:background z:0];
    
    CCSprite *bed = [CCSprite spriteWithFile:@"bed.png"];
    [bed setAnchorPoint:ccp(0.5,1)];
    [bed setPosition:ccp((winSize.width /2),
                                (background.boundingBox.size.height/2) + 20)];
    [self addChild:bed z:2];
}


- (void) nextFrame:(ccTime)dt {
    if(self.charactor.position.x < (self.boundingBox.size.width/2) + 50){
        [self performSelector:@selector(nextScene) withObject:nil afterDelay:0.5];
    }
    self.charactor.position = ccp( self.charactor.position.x - 100*dt, self.charactor.position.y );
}

-(void)nextScene{
    [[CCDirector sharedDirector]replaceScene:[Scene3Layer scene]];
}

-(void)onEnter{
    [super onEnter];
//    CCTransitionFade *fade=[CCTransitionFade transitionWithDuration:4.0 scene:[Scene3Layer scene] withColor:ccBLACK];
//    [[CCDirector sharedDirector]replaceScene:fade];
}


@end
