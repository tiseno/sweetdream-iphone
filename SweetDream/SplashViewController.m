//
//  SplashViewController.m
//  iTennis
//
//  Created by Brandon Trebitowski on 3/18/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "SplashViewController.h"


@implementation SplashViewController

@synthesize timer,splashImageView,viewController;
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
    
    @autoreleasepool {
        [super loadView];
        self.navigationController.navigationBarHidden = YES;
        [self.view setBackgroundColor:[UIColor whiteColor]];

//        splashImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"splash.png"]];
//        splashImageView.frame = CGRectMake(0, 0, [SDAppDelegate window_height], [SDAppDelegate window_width]);
//        [self.view addSubview:splashImageView];
        
        timer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(fadeScreen) userInfo:nil repeats:NO];
    }
}

-(void) onTimer{
}

- (void)fadeScreen
{
	[UIView beginAnimations:nil context:nil]; // begins animation block
	[UIView setAnimationDuration:0.75];        // sets animation duration
	[UIView setAnimationDelegate:self];        // sets delegate for this block
	[UIView setAnimationDidStopSelector:@selector(finishedFading)];   // calls the finishedFading method when the animation is done (or done fading out)	
	self.view.alpha = 0.0;       // Fades the alpha channel of this view to "0.0" over the animationDuration of "0.75" seconds
	[UIView commitAnimations];   // commits the animation block.  This Block is done.
}


- (void) finishedFading
{
	[UIView beginAnimations:nil context:nil]; // begins animation block
	[UIView setAnimationDuration:0.75];        // sets animation duration
	self.view.alpha = 1.0;   // fades the view to 1.0 alpha over 0.75 seconds
//	viewController.view.alpha = 1.0;
	[UIView commitAnimations];   // commits the animation block.  This Block is done.
	[splashImageView removeFromSuperview];
    
    UINavigationController *uc = [self.storyboard instantiateViewControllerWithIdentifier:@"navigationController"];
    
    [self presentViewController:uc animated:NO completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

@end
