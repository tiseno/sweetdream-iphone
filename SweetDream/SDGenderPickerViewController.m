//
//  SDGenderPickerViewController.m
//  SweetDream
//
//  Created by desmond on 13-3-4.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import "SDGenderPickerViewController.h"

@interface SDGenderPickerViewController ()
@end

@implementation SDGenderPickerViewController
@synthesize navItem,scrollView,hintLabel;
@synthesize rootUIView,elementView,clockArea;
@synthesize arrow,genderPickLayer;
@synthesize titleLabel,title_background;

- (void)viewDidLoad
{
    @autoreleasepool {
        [hintLabel setFont:[UIFont fontWithName:@"CreteRound-Regular" size:hintLabel.font.pointSize]];
        
        [self.navItem init];
        self.navigationController.navigationBar.hidden = NO;
        self.navItem.rightBarButtonItem.enabled =NO;
        self.navItem.leftBarButtonItem.enabled =NO;
        
        self.scrollView.contentSize = CGSizeMake(640, 360);
        self.scrollView.delegate = self;

        arrow = [[SDPointerArrowView alloc]init];
        [self.view insertSubview:arrow aboveSubview:scrollView];
        [arrow hideLeftArrow:YES];
        
        [super viewDidLoad];
    }
}

-(NSArray*) getCharacter{
    CharacterLayer *chars = [CharacterLayer node];
    [chars setCharacterBody:@"male_normal_body.png"];
    [chars setCharactergender:0];
    [chars setContentSize:CGSizeMake(180, 440)];
    [chars setPosition:CGPointMake(320/2-(180/2), 44)];
    
    CharacterLayer *chars2 = [CharacterLayer node];
    [chars2 setCharacterBody:@"female_normal_body.png"];
    [chars2 setContentSize:CGSizeMake(180, 440)];
    [chars2 setCharactergender:1];
    [chars2 setPosition:CGPointMake(960/2-(180/2), 44)];
    
    return [NSArray arrayWithObjects:chars, chars2, nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.clockArea addSubview:[ClockView getClockView]];    

    genderPickLayer = [GenderPickerLayer alloc];

    [[CCDirector sharedDirector] runWithScene:[genderPickLayer scene]];
    [genderPickLayer insertCharacter:[self getCharacter] AndNeedHint:NO];
    [[CCDirector sharedDirector] startAnimation];
    [SDCustomLayerDelegate getCustomLayerManager].layerDelegate = self;
    
    [self.scrollView addSubview:[CCDirector sharedDirector].view ];

    [[ClockView getClockView]start];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    
    if([SDUserData sharedSDUserData].gender != -1){
        self.navItem.rightBarButtonItem.enabled = YES;
        [self.navItem.rightButton addTarget:self action:@selector(handleNext:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    //bottom title animation
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height - 44;
    [[SDSimpleAnimation getAnimationManager] doAnimation:titleLabel startPoint:CGPointMake(20, screenHeight) maxPoint:CGPointMake(20, screenHeight - 72) endPoint:CGPointMake(20, screenHeight - 69) AndDirectionIsUP:YES];
    
    [[SDSimpleAnimation getAnimationManager] doAnimation:title_background startPoint:CGPointMake(2, screenHeight) maxPoint:CGPointMake(2, 439) endPoint:CGPointMake(2, 441) AndDirectionIsUP:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[CCDirector sharedDirector] stopAnimation];
    [[CCDirector sharedDirector] popScene];
    [self setGenderPickLayer:nil];
    [[CCDirector sharedDirector].view removeFromSuperview];
    [SDCustomLayerDelegate getCustomLayerManager].layerDelegate = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void) handleNext:(id)sender
{
    [self performSegueWithIdentifier:@"SleepPosistionSugue" sender:self];
}

- (void)findOutTheTag:(id)sender {
        [[SDSimpleAnimation getAnimationManager] doZoomInOutAnimation:((UIGestureRecognizer *)sender).view startScalePoint:CGPointMake(1.05, 1.05) EndScalePoint:CGPointMake(1.0, 1.0)];
        [SDSimpleAnimation getAnimationManager].animaDelegate = self;
        
        int gender_ = -1;
        switch (((UIGestureRecognizer *)sender).view.tag)
        {
            case 0:
                gender_ = 0;
                break;

            case 1:
                gender_ = 1;
                break;
        }
        [SDUserData sharedSDUserData].gender = gender_;
    
}

#pragma UIScrollView Delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    float offset = scrollView.contentOffset.x;
    if(offset < 320){
        [arrow hideLeftArrow:YES];
        [arrow hideRightArrow:NO];
    }else if(offset >= scrollView.contentSize.width - 320){
        [arrow hideRightArrow:YES];
        [arrow hideLeftArrow:NO];
    }else{
        [arrow hideLeftArrow:NO];
        [arrow hideRightArrow:NO];
    }
}

#pragma SDSimpleAnimation Delegate
-(void)didEndAnimation:(BOOL)result{
    [self performSegueWithIdentifier:@"SleepPosistionSugue" sender:self];
}

#pragma GenderPickerLayer Delegate
- (void)ItemDidSelect:(NSInteger)result{
    if (result == 99) {
        [self showAlert];
    }else{
    [SDUserData sharedSDUserData].gender = result;
    [self performSegueWithIdentifier:@"SleepPosistionSugue" sender:self];
    }
}

-(void)showAlert{
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Customer Care Hotline" message:@"1300-88-9921" delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:@"Call",nil];
    [alert show];
    
}

#pragma mark-
#pragma mark- UIAlertView delegate
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    NSLog(@"buttonindex %d", buttonIndex);
    if (buttonIndex == 1) {
        NSString *phoneNumber = [@"telprompt://" stringByAppendingString:@"1300889921"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    }
}


-(void)showContactNumber{
    CGRect panelFrame = CGRectMake(0, -10, self.view.bounds.size.width, self.view.bounds.size.height + 10);
    RecommendedPillowView *modalPanel = [[RecommendedPillowView alloc]initWithFrame:panelFrame title:@""];
    modalPanel.label.text = @"010 - 1234567";
    modalPanel.backgroundImg.image = [UIImage imageNamed:@"tab_pillow"];
    [self.view addSubview:modalPanel];
    [modalPanel showFromPoint:CGPointMake(160, 240)];

}


- (void)viewDidUnload {
    [self setElementView:nil];
    [self setRootUIView:nil];
    [self setClockArea:nil];
    [self setScrollView:nil];
    [self setNavItem:nil];
    [self setScrollView:nil];
    [self setHintLabel:nil];
    [self setArrow:nil];
    [self setGenderPickLayer:nil];
    [self setTitleLabel:nil];
    [self setTitle_background:nil];
    [super viewDidUnload];
}
- (IBAction)switchItem:(id)sender {
    static int scrollVIew_offset = 0;
    switch ([sender tag]) {
        case 1:
            if (!(scrollVIew_offset <= 0))
                scrollVIew_offset = scrollVIew_offset - 320;
            break;
            
        case 2:
            if (scrollVIew_offset < 320)
                scrollVIew_offset = scrollVIew_offset + 320;
            break;
    }
    [self.scrollView setContentOffset:CGPointMake(scrollVIew_offset, 0) animated:YES ];

 }

@end
