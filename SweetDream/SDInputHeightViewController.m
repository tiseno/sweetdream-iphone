//
//  SDInputHeightViewController.m
//  SweetDream
//
//  Created by desmond on 13-3-6.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import "SDInputHeightViewController.h"
#define onesComponent 2
#define tensComponent 1
#define hundredsComponent 0

@interface SDInputHeightViewController ()

@end

@implementation SDInputHeightViewController
@synthesize hintLabel,scrollView,navItem,actionSheet,picker, clockArea;
@synthesize ones, tens, hundreds;
@synthesize genderLayer;
@synthesize titleLabel,title_background;

BOOL enteredHeight = false; //if false then show picker

- (void)viewDidLoad
{
    @autoreleasepool {
        
        [super viewDidLoad];
        [hintLabel setFont:[UIFont fontWithName:@"CreteRound-Regular" size:hintLabel.font.pointSize]];
        [navItem init];
        navItem.rightBarButtonItem.enabled = NO;
        [navItem.leftButton addTarget:self action:@selector(handlePrev:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.scrollView setDelegate:self];
        [self.scrollView setContentOffset:CGPointMake(320, 0) animated:NO];
        [self.scrollView setScrollEnabled:NO];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(findOutTheTag:)];
        [self.scrollView addGestureRecognizer:tap];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    genderLayer = [GenderPickerLayer alloc];

    [[CCDirector sharedDirector] runWithScene:[genderLayer scene]];
    [[CCDirector sharedDirector] startAnimation];
    [genderLayer insertCharacter:[self getCharacter] AndNeedHint:YES];
    
    [self.scrollView addSubview:[CCDirector sharedDirector].view ];
    self.scrollView.contentSize = CGSizeMake(960, 360);

    [super viewDidAppear:NO];
    [self.clockArea addSubview:[ClockView getClockView]];
    [[ClockView getClockView]start];
    
    enteredHeight = NO;
    if([SDUserData sharedSDUserData].height != -1){
        self.navItem.rightBarButtonItem.enabled = YES;
        [self.navItem.rightButton addTarget:self action:@selector(handleNext:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    //bottom title animation
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height - 44;
    [[SDSimpleAnimation getAnimationManager] doAnimation:titleLabel startPoint:CGPointMake(20, screenHeight) maxPoint:CGPointMake(20, screenHeight - 72) endPoint:CGPointMake(20, screenHeight - 69) AndDirectionIsUP:YES];
    
    [[SDSimpleAnimation getAnimationManager] doAnimation:title_background startPoint:CGPointMake(2, screenHeight) maxPoint:CGPointMake(2, 439) endPoint:CGPointMake(2, 441) AndDirectionIsUP:YES];
}


-(NSArray*) getCharacter{
    NSString *gender = @"male_";
    
    if([SDUserData sharedSDUserData].gender == 1){
        gender = @"female_";
    }else{
        gender = @"male_";
    }
    
    CharacterLayer *chars = [CharacterLayer node];
    [chars setCharacterBody:[NSString stringWithFormat:@"%@short_body.png",gender]];
    [chars setCharactergender:[SDUserData sharedSDUserData].gender];
    [chars setContentSize:CGSizeMake(180, 440)];
    [chars setPosition:CGPointMake(320/2-(180/2), 44)];
    
    CharacterLayer *chars2 = [CharacterLayer node];
    [chars2 setCharacterBody:[NSString stringWithFormat:@"%@normal_body.png",gender]];
    [chars2 setCharactergender:[SDUserData sharedSDUserData].gender];
    [chars2 setContentSize:CGSizeMake(180, 440)];
    [chars2 setPosition:CGPointMake(960/2-(180/2), 44)];
    
    CharacterLayer *chars3 = [CharacterLayer node];
    [chars3 setCharacterBody:[NSString stringWithFormat:@"%@tall_body.png",gender]];
    [chars3 setCharactergender:[SDUserData sharedSDUserData].gender];
    [chars3 setContentSize:CGSizeMake(180, 440)];
    [chars3 setPosition:CGPointMake(1600/2-(180/2), 44)];
    
    return [NSArray arrayWithObjects:chars, chars2,chars3 ,nil];
}

-(void)initScrollView{
    NSMutableArray *characters = [[NSMutableArray alloc] init];
    
    if([SDUserData sharedSDUserData].gender == 1){
        [characters addObject:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"female_short"]]];
        [characters addObject:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"female_normal"]]];
        [characters addObject:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"female_tall"]]];
    }else{
        [characters addObject:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"male_short"]]];
        [characters addObject:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"male_normal"]]];
        [characters addObject:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"male_tall"]]];

    }
    
    for (int i = 0; i < characters.count; i++) {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(findOutTheTag:)];
        [[characters objectAtIndex:i] addGestureRecognizer:tap];
    }
    
    self.scrollView = [[SDPickerScrollView alloc] initWithImageArray:characters AndScrollView:self.scrollView AndItemFrame:CGRectMake(70, 0, 180, 440)] ;
    [self.scrollView setDelegate:self];
    [self.scrollView setContentOffset:CGPointMake(320, 0) animated:NO];
    [self.scrollView setScrollEnabled:NO];
}

#pragma handle when tap in image
- (void)findOutTheTag:(id)sender {
    [self showPicker];
}

#pragma Delegate UIScrollView
-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    if (enteredHeight) {
        sleep(1.5);
        [self performSegueWithIdentifier:@"BmiResultSugue" sender:self];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) handlePrev:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) handleNext:(id)sender
{
//    [self performSegueWithIdentifier:@"ResultSugue" sender:self];
}

- (void) showPicker{
    [[CCDirector sharedDirector] stopAnimation];
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                              delegate:nil
                                     cancelButtonTitle:nil
                                destructiveButtonTitle:nil
                                     otherButtonTitles:nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    CGRect pickerFrame = CGRectMake(0, 40, 0, 0);
    
    picker = [[UIPickerView alloc] initWithFrame:pickerFrame];
    picker.showsSelectionIndicator = YES;
    picker.dataSource = self;
    picker.delegate = self;
    [picker selectRow:ones inComponent:onesComponent animated:NO];
    [picker selectRow:tens inComponent:tensComponent animated:NO];
    [picker selectRow:hundreds inComponent:hundredsComponent animated:NO];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(265, 137, 320, 21)];
    label.text = @"cm";
    label.font = [UIFont systemFontOfSize:20];
    label.backgroundColor = [UIColor clearColor];
    label.shadowColor = [UIColor lightGrayColor];
    label.shadowOffset = CGSizeMake (0,1);

    [actionSheet addSubview:picker];
    [actionSheet addSubview:label];
    
    UISegmentedControl *closeButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Done"]];
    closeButton.momentary = YES;
    closeButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
    closeButton.segmentedControlStyle = UISegmentedControlStyleBar;
    closeButton.tintColor = [UIColor blackColor];
    [closeButton addTarget:self action:@selector(dismissActionSheet) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:closeButton];
    
    [actionSheet showInView:[[UIApplication sharedApplication] keyWindow]];
    
    [actionSheet setBounds:CGRectMake(0, 0, 320, 485)];
}

-(void)dismissActionSheet{
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    enteredHeight = true;
    //switch scrollView position
    
    NSInteger height_ = ones + tens*10 + hundreds*100;
    
    if(height_ <= 0){
        [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
        [self showAlert];
        return;
    }

    [SDUserData sharedSDUserData].height = height_;
    CGPoint scrollVIew_offset = [self getScrollingOffset:[SDUserData sharedSDUserData].height];
    
    [self.scrollView setContentOffset:scrollVIew_offset animated:YES ];
    
    if(self.scrollView.contentOffset.x == scrollVIew_offset.x){
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            if (enteredHeight) {
                sleep(1.5);
                dispatch_async(dispatch_get_main_queue(),^{
                    [self performSegueWithIdentifier:@"BmiResultSugue" sender:self ];
                });
            }
        });
    }
}

-(void)showAlert{
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Height Error" message:@"Please enter Your height." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
}

#pragma mark-
#pragma mark- UIAlertView delegate
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    [self performSelector:@selector(showPicker) withObject:nil afterDelay:0.4];
    
}

-(CGPoint)getScrollingOffset:(NSInteger) charHeight{
    CGPoint scrollVIew_offset = CGPointMake(0, 0);
    if (charHeight <= 155) {
        scrollVIew_offset = CGPointMake(0, 0);
        
    }else if(charHeight >= 156 && charHeight <= 170){
        scrollVIew_offset = CGPointMake(320, 0);
        
    }else if (charHeight >= 171){
        scrollVIew_offset = CGPointMake(640, 0);
    }
    return scrollVIew_offset;
}

#pragma mark-
#pragma mark Picker Data Source Methods
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 3;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    switch (component) {
        case hundredsComponent:
            return 3;
            break;
            
        case tensComponent:
            return 10;
            break;
            
        case onesComponent:
            return 10;
            break;
    }
    
    return 10;
}

#pragma mark Picker Delegate Methods
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [[NSString alloc] initWithFormat:@"%d",row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    switch (component) {
        case hundredsComponent:
            hundreds = row;
            break;
            
        case tensComponent:
            tens = row;
            break;
            
        case onesComponent:
            ones = row;
            break;
    }
    
}

-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    switch (component) {
        case hundredsComponent:
            return 100;
            break;
            
        case tensComponent:
            return 100;
            break;
            
        case onesComponent:
            return 120;
            break;
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    [[CCDirector sharedDirector] stopAnimation];
    [[CCDirector sharedDirector] popScene];
    [self setGenderLayer:nil];
    [[CCDirector sharedDirector].view removeFromSuperview];
    
    //    [super viewDidDisappear:animated];
}

- (void)viewDidUnload {
    [self setClockArea:nil];
    [super viewDidUnload];
}
@end
