//
//  SDSimpleAnimation.m
//  SweetDream
//
//  Created by desmond on 13-3-7.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import "SDSimpleAnimation.h"

@implementation SDSimpleAnimation
@synthesize animaDelegate,delegateIdentified;

static SDSimpleAnimation *manager = nil;

+(SDSimpleAnimation* ) getAnimationManager{
    
    @synchronized([SDSimpleAnimation class]) {
        if (!manager)
           manager =  [[self alloc] init];
        
        return manager;
    }
    
    return nil;
}

+ (id)alloc {
    @synchronized([SDSimpleAnimation class]) {
        
        NSAssert(manager == nil, @"manager already alloc");
        manager = [super alloc];
        
        return manager;
    }
    return nil;
}

-(void)doAnimation:(UIView *) view startPoint:(CGPoint)start maxPoint:(CGPoint)max endPoint:(CGPoint)end AndDirectionIsUP:(BOOL) directionIsUP;{
    int offset = directionIsUP ? 1 : -1 ;
    
    CGRect viewFrame = [view frame];
    viewFrame.origin.x  = start.x;
    viewFrame.origin.y  = start.y;
    view.frame = viewFrame;
    view.alpha = 1.0;
    
    [UIView animateWithDuration:.4 animations:^() {
        CGRect viewFrame = [view frame];
        viewFrame.origin.x  = max.x;
        viewFrame.origin.y  = max.y;
        view.frame = viewFrame;
        view.alpha = 1.0;
        
    }completion:^(BOOL finished) {
        [UIView animateWithDuration:.3 animations:^() {
            CGRect viewFrame = [view frame];
            viewFrame.origin.x  = end.x;
            viewFrame.origin.y  = end.y + 4 * offset;
            view.frame = viewFrame;
            view.alpha = 1.0;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:.4 animations:^() {
                CGRect viewFrame = [view frame];
                viewFrame.origin.x  = end.x;
                viewFrame.origin.y  = end.y;
                view.frame = viewFrame;
                view.alpha = 1.0;
                [animaDelegate didEndAnimation:delegateIdentified];
                delegateIdentified = -1;
            }
             ];
        }
         ];
    }];
    
}

-(void)leftPointerAnimation:(UIView *) view startPoint:(CGPoint)start maxPoint:(CGPoint)max endPoint:(CGPoint)end AndDirectionIsRight:(BOOL) directionIsRight{
    int offset = directionIsRight ? -1 : 1 ;
    
    CGRect viewFrame = [view frame];
    viewFrame.origin.x  = start.x;
    viewFrame.origin.y  = start.y;
    view.frame = viewFrame;
//    view.alpha = 1.0;
    
    [UIView animateWithDuration:.2 animations:^() {
        CGRect viewFrame = [view frame];
        viewFrame.origin.x  = max.x;
        viewFrame.origin.y  = max.y;
        view.frame = viewFrame;
//        view.alpha = 1.0;
        
    }completion:^(BOOL finished) {
        [UIView animateWithDuration:.3 animations:^() {
            CGRect viewFrame = [view frame];
            viewFrame.origin.x  = end.x + 4 * offset;
            viewFrame.origin.y  = end.y;
            view.frame = viewFrame;
//            view.alpha = 1.0;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:.4 animations:^() {
                CGRect viewFrame = [view frame];
                viewFrame.origin.x  = end.x;
                viewFrame.origin.y  = end.y;
                view.frame = viewFrame;
//                view.alpha = 1.0;
            }
             ];
        }
         ];
    }];
    
}

-(void)doZoomInOutAnimation:(UIView *) view startScalePoint:(CGPoint)start EndScalePoint:(CGPoint)end{
    [UIView animateWithDuration:.1 animations:^() {
        view.transform = CGAffineTransformMakeScale(start.x, start.y);
        
    }completion:^(BOOL finished) {
        [UIView animateWithDuration:.1 animations:^() {
            view.transform = CGAffineTransformMakeScale(end.x - (start.x * 0.01), end.y - (start.y * 0.01));
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:.1 animations:^() {
                view.transform = CGAffineTransformMakeScale(end.x, end.y);
            } completion:^(BOOL finished) {
                
                [animaDelegate didEndAnimation:delegateIdentified];
                delegateIdentified = -1;
            }
             ];
        }
         ];
    }];
}

-(void)doSpinningAnimation:(UIView *) view Origin:(float) OriDegree Destination:(float)DesDegree inTime:(float)time popEffect:(BOOL) effect{
    [CATransaction begin];
    CAKeyframeAnimation *rotationAnimation;
    rotationAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation.z"];
    
    rotationAnimation.values = [NSArray arrayWithObjects:
                                [NSNumber numberWithFloat:OriDegree/180 * M_PI],
                                [NSNumber numberWithFloat:DesDegree/180 * M_PI], nil];
    rotationAnimation.calculationMode = kCAAnimationPaced;
    
    rotationAnimation.removedOnCompletion = NO;
    rotationAnimation.fillMode = kCAFillModeForwards;
    
    rotationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    rotationAnimation.duration = time;
    [CATransaction setCompletionBlock:^{
        if(! effect)
            return;
        
        self.animaDelegate = nil;
        [self doZoomInOutAnimation:view startScalePoint:CGPointMake(1.05, 1.05) EndScalePoint:CGPointMake(1.0, 1.0)];
    }];
    CALayer *layer = [view layer];
    
    [layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];

    [CATransaction commit];
}

@end
