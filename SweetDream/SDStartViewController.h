//
//  SDStartViewController.h
//  SweetDream
//
//  Created by desmond on 13-3-4.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDSimpleAnimation.h"
#import "SDUserData.h"
#import "cocos2d.h"

#import <FacebookSDK/FacebookSDK.h>
#import <Social/Social.h>

@interface SDStartViewController : UIViewController<UIActionSheetDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *paperRow;
@property (strong, nonatomic) IBOutlet UIView *animaView;
@property (strong, nonatomic) IBOutlet UILabel *introLabel;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (strong, nonatomic) IBOutlet UIView *mainView2;


- (IBAction)handlePan:(UIPanGestureRecognizer *)recognizer;

@end
