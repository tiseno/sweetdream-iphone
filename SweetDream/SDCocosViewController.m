//
//  SDCocosViewController.m
//  SweetDream
//
//  Created by desmond on 13-3-19.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import "SDCocosViewController.h"

@interface SDCocosViewController ()

@end

@implementation SDCocosViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	 [CCDirector sharedDirector].view.frame = CGRectMake(0, 0, 320, 480);
//     [[CCDirector sharedDirector] runWithScene:[GenderPickerLayer scene]];
    
    [self.view addSubview:[CCDirector sharedDirector].view];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
