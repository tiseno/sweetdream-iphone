//
//  Scene1Layer.m
//  SweetDream
//
//  Created by desmond on 13-3-29.
//  Copyright 2013年 desmond. All rights reserved.
//

#import "Scene1Layer.h"


@implementation Scene1Layer
@synthesize charactor;
ColorBar *gradientLayer;
+(CCScene *) scene
{
    @autoreleasepool {
        // 'scene' is an autorelease object.
        CCScene *scene = [CCScene node];
        
        Scene1Layer *layer = [Scene1Layer node];
        
        [scene addChild:layer];
        
        return scene;
    }
}

-(id)init{
    @autoreleasepool {
    if((self = [super init])) {
            [self contentSize].width;
            [self initBackground];
            [self initCharactor];
    //        NSLog(@"background: width = %f, height: %f",[background scaleX] ,[background boundingBox].size.height);
            
            CCLayerColor *Layer = [CCLayerColor layerWithColor:ccc4(123, 123, 211, 100)];
            [Layer setContentSize:[self contentSize]];
            [Layer setAnchorPoint:ccp(0,0)];
            [Layer setPosition:ccp(0,0)];
            
            [self schedule:@selector(nextFrame:)];
            
        }
        return self;
    }
}

-(void)initCharactor{
    NSString *gender = ([SDUserData sharedSDUserData].gender == 0)? @"": @"f_";
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:[NSString stringWithFormat:
     @"%@walk.plist",gender]];
    
    CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode
                                      batchNodeWithFile:[NSString stringWithFormat:
                                                         @"%@walk.png",gender]];
    
    NSMutableArray *walkAnimFrames = [NSMutableArray array];
    for(int i =1; i <=8; ++i) {
        [walkAnimFrames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"walk_%d.png",i]]];
    }
    
    CCAnimation *walkAnim = [CCAnimation animationWithSpriteFrames:walkAnimFrames delay:0.1f];
    walkAnim.restoreOriginalFrame = YES;
    
    CGSize winSize = [CCDirector sharedDirector].winSize;
    charactor = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:
                 @"walk_1.png",gender]];
    charactor.position = ccp((winSize.width/2) + 100, (winSize.height/2)-10);
    CCAnimate *walkAction = [CCRepeatForever actionWithAction:
                       [CCAnimate actionWithAnimation:walkAnim]];
    [charactor runAction:walkAction];
    [spriteSheet addChild:charactor];

    [self addChild:spriteSheet];
}

-(void)initBackground{
    CGSize winSize = [CCDirector sharedDirector].winSize;
    CCSprite *background = [CCSprite spriteWithFile:@"bg_scene_1.png"];
    [background setAnchorPoint:ccp(0.5,0.5)];
    [background setPosition:ccp(winSize.width /2,winSize.height/2)];
    [self addChild:background];
}

- (void) nextFrame:(ccTime)dt {
    self.charactor.position = ccp( self.charactor.position.x - 100*dt, self.charactor.position.y );

    if(self.charactor.position.x < 100){
        [[CCDirector sharedDirector]replaceScene:[Scene2Layer scene]];
        charactor.position = ccp( 480+(233/2), charactor.position.y );
    }
    
}

- (void)draw
{
}

@end
