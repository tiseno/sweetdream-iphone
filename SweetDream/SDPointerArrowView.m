//
//  SDPointerArrowView.m
//  SweetDream
//
//  Created by desmond on 13-3-22.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import "SDPointerArrowView.h"

@implementation SDPointerArrowView
@synthesize arrowLeft,arrowRight;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id)init{
    self = [super init];
    if (self) {
        CGRect viewFrame = CGRectMake(0, 201, 320, 480);
        viewFrame.origin.x  = 0;
        viewFrame.origin.y  = 201;

        self.backgroundColor = [UIColor clearColor];
        self.frame= viewFrame;
        
        CGRect arrowLeftRect=CGRectMake(12, 0, 35, 35);
        CGRect arrowRightRect=CGRectMake(273, 0, 35, 35);
        
        self.arrowRight = [[UIImageView alloc] initWithFrame:arrowRightRect];
        [self.arrowRight setImage:[UIImage imageNamed:@"btn_more_right" ]];
        self.arrowRight.userInteractionEnabled = YES;
        
        self.arrowLeft= [[UIImageView alloc] initWithFrame:arrowLeftRect];
        [self.arrowLeft setImage:[UIImage imageNamed:@"btn_more_left" ]];
        self.arrowLeft.userInteractionEnabled = YES;
        
        [self addSubview:self.arrowLeft];
        [self addSubview:self.arrowRight];
        
        NSTimer *myTimer = [NSTimer scheduledTimerWithTimeInterval: 2
                                                            target: self
                                                          selector: @selector(showAnimation)
                                                          userInfo: nil
                                                           repeats: YES];
    }
    return self;
}

-(void)showAnimation{
    
    [[SDSimpleAnimation getAnimationManager] leftPointerAnimation:self.arrowRight startPoint:CGPointMake(273, 0) maxPoint:CGPointMake(285, 0) endPoint:CGPointMake(273, 0) AndDirectionIsRight:YES];
    
    [[SDSimpleAnimation getAnimationManager] leftPointerAnimation:self.arrowLeft startPoint:CGPointMake(11, 0) maxPoint:CGPointMake(0, 0) endPoint:CGPointMake(11, 0) AndDirectionIsRight:NO];
}

-(void)hideLeftArrow:(BOOL) flag{
    if(flag)
        self.arrowLeft.alpha = 0;
    else
        self.arrowLeft.alpha = 1;
}

-(void)hideRightArrow:(BOOL) flag{
    if(flag)
        self.arrowRight.alpha = 0;
    else
        self.arrowRight.alpha = 1;
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {

    return NO;
}

@end
