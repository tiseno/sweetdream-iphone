//
//  SDPickerScrollView.h
//  SweetDream
//
//  Created by desmond on 13-3-5.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ItemTapDelegate <NSObject>
    @required
    - (void)findOutTheTag:(id)sender;
@end


@interface SDPickerScrollView : UIScrollView
@property (retain, nonatomic) id<ItemTapDelegate> tapDelegate;

-(id)initWithImageArray:(NSArray *) arry AndScrollView:(UIScrollView *)scrollView AndItemFrame:(CGRect) ItemFrame;

-(void)findOutTheTag:(id)sender;
-(void)scrollToPage:(NSInteger) pageNumber;
@end
