//
//  SDUserData.m
//  SweetDream
//
//  Created by desmond on 13-3-4.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import "SDUserData.h"

@implementation SDUserData
@synthesize gender,height,weight,sleppingPosition,firmness;
@synthesize supportFactor,bmi,recommendedPillow,supportFactorNsleepPosture,supportFactorValue;

static SDUserData *UserData = nil;


- (id)init {
    self = [super init];
    if (self) {
        gender = -1;
        height = -1;
        weight = -1;
        sleppingPosition = -1;
        firmness = -1;
        bmi = 0.0;
        recommendedPillow = @"NO";
        supportFactor = @"normal";
        supportFactorValue = @"0";
        supportFactorNsleepPosture = @"You are Over Weight\n LateralPosture(side sleeper)";
    }
    
    return self;
}

+ (SDUserData *)sharedSDUserData{
    
    @synchronized([SDUserData class]) {
        if (!UserData)
            [[self alloc] init];
        
        return UserData;
    }
    
    return nil;
}

+ (id)alloc {
    @synchronized([SDUserData class]) {
        
        NSAssert(UserData == nil, @"UserData already alloc");
        UserData = [super alloc];
        
        return UserData;
    }
    return nil;
}


@end
