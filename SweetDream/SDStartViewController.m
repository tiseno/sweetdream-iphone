//
//  SDStartViewController.m
//  SweetDream
//
//  Created by desmond on 13-3-4.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import "SDStartViewController.h"

@interface SDStartViewController ()

@end

@implementation SDStartViewController
@synthesize paperRow,animaView,introLabel;

//test
@synthesize mainView,mainView2;

- (void)viewDidLoad
{
    NSLog(@"Available fonts: %@", [UIFont familyNames]);
    [super viewDidLoad];
    [introLabel setFont:[UIFont fontWithName:@"CreteRound-Regular" size:introLabel.font.pointSize]];
    self.navigationController.navigationBar.hidden = NO;
    }

-(void)initCCDirector{
    CGRect frame = CGRectMake(0, 0, 960, 480);
    // Create an CCGLView with a RGB565 color buffer, and a depth buffer of 0-bits
	CCGLView *glView = [CCGLView viewWithFrame:frame
								   pixelFormat:kEAGLColorFormatRGBA8	//kEAGLColorFormatRGBA8
								   depthFormat:0	//GL_DEPTH_COMPONENT24_OES
							preserveBackbuffer:NO
									sharegroup:nil
								 multiSampling:NO
							   numberOfSamples:0];
    
    [glView setMultipleTouchEnabled:YES];
	glView.opaque = NO;
    
    CCDirector *director_;
    
	director_ = (CCDirectorIOS*) [CCDirector sharedDirector];
    
    //	[director_ initGLDefaultValues:glView];
	director_.wantsFullScreenLayout = YES;
	
	// Display FSP and SPF
	[director_ setDisplayStats:NO];
	
	// set FPS at 60
	[director_ setAnimationInterval:1.0/60];
	
	// attach the openglView to the director
	[director_ setView:glView];
	
	// 2D projection
	[director_ setProjection:kCCDirectorProjection2D];
	//	[director setProjection:kCCDirectorProjection3D];
	
	// Enables High Res mode (Retina Display) on iPhone 4 and maintains low res on all other devices
	if( ! [director_ enableRetinaDisplay:YES] )
		CCLOG(@"Retina Display Not supported");
	
	// Default texture format for PNG/BMP/TIFF/JPEG/GIF images
	// It can be RGBA8888, RGBA4444, RGB5_A1, RGB565
	// You can change this setting at any time.
	[CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA8888];
	
	// If the 1st suffix is not found and if fallback is enabled then fallback suffixes are going to searched. If none is found, it will try with the name without suffix.
	// On iPad HD  : "-ipadhd", "-ipad",  "-hd"
	// On iPad     : "-ipad", "-hd"
	// On iPhone HD: "-hd"
	CCFileUtils *sharedFileUtils = [CCFileUtils sharedFileUtils];
	[sharedFileUtils setEnableFallbackSuffixes:NO];				// Default: NO. No fallback suffixes are going to be used
	[sharedFileUtils setiPhoneRetinaDisplaySuffix:@"-hd"];		// Default on iPhone RetinaDisplay is "-hd"
	[sharedFileUtils setiPadSuffix:@"-ipad"];					// Default on iPad is "ipad"
	[sharedFileUtils setiPadRetinaDisplaySuffix:@"-ipadhd"];	// Default on iPad RetinaDisplay is "-ipadhd"
	
	// Assume that PVR images have premultiplied alpha
	[CCTexture2D PVRImagesHavePremultipliedAlpha:YES];
    
    [[CCDirector sharedDirector] runWithScene:[CCScene node]];
    [[CCDirector sharedDirector] stopAnimation];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [[SDUserData sharedSDUserData] init];
    [[SDSimpleAnimation getAnimationManager] doAnimation:animaView startPoint:CGPointMake(7, -175) maxPoint:CGPointMake(7, 0) endPoint:CGPointMake(7, -11) AndDirectionIsUP:NO];
    [self initCCDirector];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)viewDidUnload {
    [self setPaperRow:nil];
    [self setAnimaView:nil];
    [self setIntroLabel:nil];
    [self setMainView:nil];
    [self setMainView2:nil];
    [super viewDidUnload];
}

- (IBAction)handlePan:(UIPanGestureRecognizer *)recognizer {
    if(recognizer.state == UIGestureRecognizerStateEnded)
    {
        if(recognizer.view.frame.origin.y < - 11){
            [[SDSimpleAnimation getAnimationManager] doAnimation:recognizer.view startPoint:CGPointMake(7, recognizer.view.frame.origin.y) maxPoint:CGPointMake(7, 0) endPoint:CGPointMake(7, -11) AndDirectionIsUP:NO];
        }else
            [[SDSimpleAnimation getAnimationManager] doAnimation:recognizer.view startPoint:CGPointMake(7, recognizer.view.frame.origin.y) maxPoint:CGPointMake(7, recognizer.view.frame.origin.y - 16) endPoint:CGPointMake(7, -11) AndDirectionIsUP:YES];
    }else{
        CGPoint translation = [recognizer translationInView:self.view];
        float yPoint = recognizer.view.frame.origin.y < 0 ? recognizer.view.center.y + translation.y : 181;
        
        recognizer.view.center = CGPointMake(recognizer.view.center.x,
                                             yPoint);
        [recognizer setTranslation:CGPointMake(0, 0) inView:self.view];
    }
}

@end
