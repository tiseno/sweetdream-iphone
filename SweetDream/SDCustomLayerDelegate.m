//
//  SDCustomLayerDelegate.m
//  SweetDream
//
//  Created by desmond on 13-3-20.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import "SDCustomLayerDelegate.h"

@implementation SDCustomLayerDelegate
@synthesize layerDelegate;

//static id<CustomLayerDelegate> delegate = nil;
static SDCustomLayerDelegate *manager = nil;

-(id<CustomLayerDelegate>) getCustomLayerDelegate{
    if (layerDelegate != NULL) {
        return layerDelegate;
    }else
        return nil;
}

+(SDCustomLayerDelegate* ) getCustomLayerManager{
    @synchronized([SDCustomLayerDelegate class]) {
        if (!manager)
            manager =  [[self alloc] init];
        
        return manager;
    }

return nil;
}

+ (id)alloc {
    @synchronized([SDCustomLayerDelegate class]) {
        
        NSAssert(manager == nil, @"manager already alloc");
        manager = [super alloc];
        
        return manager;
    }
    return nil;
}

@end
