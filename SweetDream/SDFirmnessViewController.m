//
//  SDFirmnessViewController.m
//  SweetDream
//
//  Created by desmond on 13-3-6.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import "SDFirmnessViewController.h"

@interface SDFirmnessViewController ()

@end

@implementation SDFirmnessViewController
@synthesize navItem, hintLabel,scrollView;
BOOL isTaped = NO;

- (void)viewDidLoad
{
    @autoreleasepool {

        [super viewDidLoad];
        [hintLabel setFont:[UIFont fontWithName:@"Lobster1.4" size:hintLabel.font.pointSize]];
        [navItem init];
        navItem.rightBarButtonItem.enabled = NO;
        [navItem.leftButton addTarget:self action:@selector(handlePrev:) forControlEvents:UIControlEventTouchUpInside];
     
        [self initScrollView];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    isTaped = NO;
    [super viewWillAppear:animated];
    if([SDUserData sharedSDUserData].firmness != -1){
        self.navItem.rightBarButtonItem.enabled = YES;
        [self.navItem.rightButton addTarget:self action:@selector(handleNext:) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)initScrollView{
    NSMutableArray *characters = [[NSMutableArray alloc] init];
    [characters addObject:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_medium_soft"]]];
    [characters addObject:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_medium_firm"]]];
    [characters addObject:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_firm"]]];
    
    for (int i = 0; i < characters.count; i++) {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(findOutTheTag:)];
        [[characters objectAtIndex:i] addGestureRecognizer:tap];
    }
    
    self.scrollView = [[SDPickerScrollView alloc] initWithImageArray:characters AndScrollView:self.scrollView AndItemFrame:CGRectMake(43, 130, 234, 175)] ;
    
    [self.scrollView scrollToPage:[SDUserData sharedSDUserData].firmness];

}

#pragma handle when tap in image
- (void)findOutTheTag:(id)sender {
    if (!isTaped) {
        CATransition *animation;
        switch (((UIGestureRecognizer *)sender).view.tag)
        {
            case 0:
//                animation = [CATransition animation];
//                [animation setDelegate:self];
//                [animation setDuration:1.5];
//                //    [animation setTimingFunction:[CAMediaTimingFunction];
//                [animation setType:@"rippleEffect"];
//                [((UIGestureRecognizer *)sender).view.layer addAnimation:animation forKey:NULL];
                
                [[SDSimpleAnimation getAnimationManager] doZoomInOutAnimation:((UIGestureRecognizer *)sender).view startScalePoint:CGPointMake(1.09, 1.09) EndScalePoint:CGPointMake(1.0, 1.0)];
                [SDSimpleAnimation getAnimationManager].animaDelegate = self;
                [SDUserData sharedSDUserData].firmness = 0 ;
                break;
                
            case 1:
                [[SDSimpleAnimation getAnimationManager] doZoomInOutAnimation:((UIGestureRecognizer *)sender).view startScalePoint:CGPointMake(1.07, 1.07) EndScalePoint:CGPointMake(1.0, 1.0)];
                [SDSimpleAnimation getAnimationManager].animaDelegate = self;
                [SDUserData sharedSDUserData].firmness = 1 ;
                break;
                
            case 2:
                [[SDSimpleAnimation getAnimationManager] doZoomInOutAnimation:((UIGestureRecognizer *)sender).view startScalePoint:CGPointMake(1.01, 1.01) EndScalePoint:CGPointMake(1.0, 1.0)];
                [SDSimpleAnimation getAnimationManager].animaDelegate = self;
                 [SDUserData sharedSDUserData].firmness = 2 ;
                break;
        }

    }
}

#pragma Delegate CAAnimation
- (void)animationDidStart:(CAAnimation *)theAnimation{
    isTaped = YES;
}

- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag
{
    [self performSegueWithIdentifier:@"inputHeightSegue" sender:self];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) handlePrev:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) handleNext:(id)sender
{
    [self performSegueWithIdentifier:@"inputHeightSegue" sender:self];
}

-(void)didEndAnimation:(BOOL)result{
    [self performSegueWithIdentifier:@"inputHeightSegue" sender:self];
}

@end
