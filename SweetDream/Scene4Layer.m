//
//  Scene4Layer.m
//  SweetDream
//
//  Created by desmond on 13-4-2.
//  Copyright 2013年 desmond. All rights reserved.
//

#import "Scene4Layer.h"


@implementation Scene4Layer{
    float currentShowRect;
    int barIncresePos;
}

+(CCScene *) scene
{
    @autoreleasepool {
        // 'scene' is an autorelease object.
        CCScene *scene = [CCScene node];
        
        Scene4Layer *layer = [Scene4Layer node];
        
        [scene addChild:layer];
        
        return scene;
    }
}

-(id)init{
    
    if((self = [super init])) {
        currentShowRect = 0;
    }
    return self;
}

-(void)initBackground{
    CGSize winSize = [CCDirector sharedDirector].winSize;
    NSString *bgName = ([SDUserData sharedSDUserData].gender == 0)? @"bg_scene_4.png": @"f_bg_scene_4.png";
    CCSprite *background = [CCSprite spriteWithFile:bgName];
    [background setAnchorPoint:ccp(0.5,0.5)];
    [background setPosition:ccp(winSize.width /2,winSize.height/2)];
    [self addChild:background z:0];

    NSBundle *bundle = [NSBundle mainBundle];
    NSURL *plistURL = [bundle URLForResource:@"barvalue" withExtension:@"plist"];
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfURL:plistURL];
//    [SDUserData sharedSDUserData].supportFactor = @"obese";
    NSString * supportFactor = [SDUserData sharedSDUserData].supportFactor;
    
    supportFactor = [supportFactor isEqual: @""]?@"under":supportFactor;
    barIncresePos = 50;
//    //for
//    if([supportFactor isEqual: @"under"]){
//        barIncresePos = 50;
//    }
//    
//    if([supportFactor isEqual: @"normal"]){
//        barIncresePos = 50;
//    }
//    
//    if([supportFactor isEqual: @"over"]){
//        barIncresePos = 50;
//    }
//    
//    if([supportFactor isEqual: @"obese"]){
//        barIncresePos = 50;
//    }
    
    ColorBarsLayer *bar = [[ColorBarsLayer alloc]initWithArrayData:[dictionary objectForKey:supportFactor]];
    [bar setAnchorPoint:ccp(0.5,0.5)];
    
    if(self.boundingBox.size.width == 480)
        [bar setPosition:ccp(self.boundingBox.size.width*.10,self.boundingBox.size.height*.47)];
    else
        [bar setPosition:ccp(self.boundingBox.size.width*.15,self.boundingBox.size.height*.47)];
    
    [self addChild:bar z:1];

}

-(void)initLabelImage{
    CCSprite *title = [CCSprite spriteWithFile:@"title_weight.png"];
    [title setAnchorPoint:ccp(0.5,0.5)];
    [title setPosition:ccp([CCDirector sharedDirector].winSize.width /2,[CCDirector sharedDirector].winSize.height/2)];
    [self addChild:title z:4];
    
    

}

-(void)showArrow{
    NSInteger arrowOffset = ([CCDirector sharedDirector].winSize.width == 480)?15 : 23;
    CCSprite *arrow = [CCSprite spriteWithFile:[NSString stringWithFormat:@"support_arrow_%@.png",[SDUserData sharedSDUserData].supportFactor]];
    [arrow setAnchorPoint:ccp(0.5,0.5)];
    [arrow setPosition:ccp(([CCDirector sharedDirector].winSize.width * .5) - arrowOffset,([CCDirector sharedDirector].winSize.height * .18) + barIncresePos)];
    [self addChild:arrow z:4 tag:88];
    
    CCSprite *title_support = [CCSprite spriteWithFile:@"title_support.png"];
    [title_support setAnchorPoint:ccp(0.5,0.5)];
    [title_support setPosition:ccp([CCDirector sharedDirector].winSize.width /2,([CCDirector sharedDirector].winSize.height * .12) + barIncresePos)];
    [self addChild:title_support z:4];
    
    [self scheduleUpdate];
}

-(void)update:(ccTime)himi{
    CCSprite *sprite =(CCSprite*)[self getChildByTag:88];

    if(currentShowRect >= 60){
        currentShowRect = currentShowRect;
    }else{
         currentShowRect += himi*100;
    }
    [sprite setTextureRect:CGRectMake(0, 0, 355,currentShowRect)];
    
}

-(void)onEnter{
    [super onEnter];
    [self initBackground];
    [self initLabelImage];
    [NSTimer scheduledTimerWithTimeInterval:5.5 target:self selector:@selector(showArrow) userInfo:nil repeats:NO];
}

@end
