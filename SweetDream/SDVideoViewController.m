//
//  SDVideoViewController.m
//  SweetDream
//
//  Created by desmond on 13-3-26.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import "SDVideoViewController.h"

@interface SDVideoViewController (){
    CGFloat screenWidth;
    CGFloat screenHeight;

}

@end

@implementation SDVideoViewController
@synthesize nextButton,repeatButton,timer;

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)initGLView{
    CGRect frame = CGRectMake(0, 0, screenWidth, screenHeight);

    [[CCDirector sharedDirector].view setFrame:frame];
    [[CCDirector sharedDirector]reshapeProjection:frame.size];
    
    [[CCDirector sharedDirector]runWithScene:[Scene1Layer scene]];
    [[CCDirector sharedDirector].view setFrame:frame];
    [self.view insertSubview:[CCDirector sharedDirector].view belowSubview:nextButton ];

}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
    CGAffineTransform newTransform = CGAffineTransformMake(0.0,1.0,-1.0,0.0,0.0,0.0);
    [self.view setTransform:newTransform];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    screenWidth = screenRect.size.height;   //landscape
    screenHeight = screenRect.size.width;
    [self initGLView];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:19.0 target:self selector:@selector(goNext:) userInfo:nil repeats:NO];
}

-(void)viewWillDisappear:(BOOL)animated{
    [[CCDirector sharedDirector] stopAnimation];
    [[CCDirector sharedDirector] popScene];
    [timer invalidate];
    timer = nil;
    
}

//- (BOOL)shouldAutorotateToInterfaceOrientation: (UIInterfaceOrientation)interfaceOrientation {
//    NSLog(@"shouldAutorotateToInterfaceOrientation : %@",(interfaceOrientation == UIInterfaceOrientationLandscapeRight?@"YES":@"NO"));
//    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight);
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)Exit:(id)sender {

}
- (void)viewDidUnload {
    [self setNextButton:nil];
    [self setRepeatButton:nil];
    [super viewDidUnload];
}
- (IBAction)repeat:(id)sender {
    [[CCDirector sharedDirector] popScene];
    [[CCDirector sharedDirector]runWithScene:[Scene1Layer scene]];
    repeatButton.hidden = YES;
}

-(void)goNext:(id)sender{
//    repeatButton.hidden = NO;
    [self performSegueWithIdentifier:@"ResultSegue" sender:self];
}

@end
