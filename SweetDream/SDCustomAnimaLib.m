//
//  SDCustomAnimaLib.m
//  SweetDream
//
//  Created by desmond on 13-3-20.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import "SDCustomAnimaLib.h"

@implementation SDCustomAnimaLib
@synthesize faceSpriteSheet;

static SDCustomAnimaLib *manager = nil;

+(SDCustomAnimaLib* ) getCustomAnimaLibManager{
    
    @synchronized([SDCustomAnimaLib class]) {
        if (!manager)
            manager =  [[self alloc] init];
        
        return manager;
    }
    
    return nil;
}

+ (id)alloc {
    @synchronized([SDCustomAnimaLib class]) {
        
        NSAssert(manager == nil, @"manager already alloc");
        manager = [super alloc];
        
        return manager;
    }
    return nil;
}

-(CCSpriteBatchNode *)SpriteSheetWithGender:(int) gender{
    NSString *fileType = (gender == 0) ? @"male_": @"female_";
    
    if(![[CCDirector sharedDirector] enableRetinaDisplay:YES] ){
        NSLog(@"Retina Display Not supported");
//        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:[NSString stringWithFormat:@"%@face.plist",fileType]];
//        faceSpriteSheet = [CCSpriteBatchNode batchNodeWithFile:[NSString stringWithFormat:@"%@.png",fileType]];
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"character_face.plist"];
         faceSpriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"character_face.png"];

    }else{
//        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:[NSString stringWithFormat:@"%@face@2x.plist",fileType]];
//        faceSpriteSheet = [CCSpriteBatchNode batchNodeWithFile:[NSString stringWithFormat:@"%@face@2x.png",fileType]];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"character_face@2x.plist"];
        faceSpriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"character_face@2x.png"];
    }
    
    return faceSpriteSheet;
}

+(CCSequence *)eyeBrownAnimaRight{
    id anima0 = [CCMoveBy actionWithDuration:.1 position:CGPointMake(0, 3)];
    id anima1 = [CCMoveBy actionWithDuration:1 position:CGPointMake(0, 0)];
    id anima2 = [CCMoveBy actionWithDuration:.1 position:CGPointMake(0, -3)];
    
    CCSequence *animaSequence = [CCSequence actions:anima0,anima1,anima2, nil];
    return [animaSequence copy];
}

+(CCSequence *)eyeBrownAnimaLeft{
    id anima0 = [CCMoveBy actionWithDuration:.1 position:CGPointMake(0, 3)];
    id anima1 = [CCMoveBy actionWithDuration:1 position:CGPointMake(0, 0)];
    id anima2 = [CCMoveBy actionWithDuration:.1 position:CGPointMake(0, -3)];
    
    CCSequence *animaSequence = [CCSequence actions:anima0,anima1,anima2, nil];
    return [animaSequence copy];
}

+(CCSequence *)headAnima{
    id anima0 = [CCRotateTo actionWithDuration:0.5 angle:5.0];
    id anima1 = [CCRotateTo actionWithDuration:0.5 angle:5.0];
    id anima2 = [CCRotateTo actionWithDuration:0.5 angle:0.0];
    CCSequence *animaSequence = [CCSequence actions:anima0,anima1,anima2, nil];
    return animaSequence;
}

-(CCAnimate *)mouthAnima1 :(int) gender{
    NSString *fileType = (gender == 0) ? @"male_": @"female_";
    NSMutableArray *walkAnimFrames = [NSMutableArray array];
    [walkAnimFrames addObject:
     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
      [NSString stringWithFormat:@"%@mouth_%d.png",fileType, 2]]];
    [walkAnimFrames addObject:
     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
      [NSString stringWithFormat:@"%@mouth_%d.png",fileType, 1]]];
    
    CCAnimation *mouthAnim = [CCAnimation animationWithSpriteFrames:walkAnimFrames delay:1.5f];
    mouthAnim.restoreOriginalFrame = YES;

    return [CCAnimate actionWithAnimation:mouthAnim];
}

-(CCAnimate *)mouthAnima2 :(int) gender{
    NSString *fileType = (gender == 0) ? @"male_": @"female_";
    NSMutableArray *walkAnimFrames = [NSMutableArray array];
    [walkAnimFrames addObject:
     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
      [NSString stringWithFormat:@"%@mouth_%d.png",fileType , 3]]];
    [walkAnimFrames addObject:
     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
      [NSString stringWithFormat:@"%@mouth_%d.png",fileType, 1]]];
    
    CCAnimation *mouthAnim = [CCAnimation animationWithSpriteFrames:walkAnimFrames delay:1.5f];
    mouthAnim.restoreOriginalFrame = YES;
    
    return [CCAnimate actionWithAnimation:mouthAnim];
}

-(CCAnimate *)mouthAnima3:(int) gender{
    NSString *fileType = (gender == 0) ? @"male_": @"female_";
    NSMutableArray *walkAnimFrames = [NSMutableArray array];
    [walkAnimFrames addObject:
     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
      [NSString stringWithFormat:@"%@mouth_%d.png",fileType, 4]]];
    [walkAnimFrames addObject:
     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
      [NSString stringWithFormat:@"%@mouth_%d.png",fileType, 1]]];
    
    CCAnimation *mouthAnim = [CCAnimation animationWithSpriteFrames:walkAnimFrames delay:1.5f];
    mouthAnim.restoreOriginalFrame = YES;
    
    return [CCAnimate actionWithAnimation:mouthAnim];
}

-(CCAnimate *)blinkEyeAnima :(int) gender{
    NSString *fileType = (gender == 0) ? @"male_": @"female_";
    NSMutableArray *eyeAnimFrames = [NSMutableArray array];
    [eyeAnimFrames addObject:
     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
      [NSString stringWithFormat:@"%@eye_%d.png", fileType, 2]]];
    [eyeAnimFrames addObject:
     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
      [NSString stringWithFormat:@"%@eye_%d.png", fileType,1]]];
    
    NSLog(@"filename= %@ ", [NSString stringWithFormat:@"%@eye_%d.png", fileType,1]);
    
    CCAnimation *Anim = [CCAnimation animationWithSpriteFrames:eyeAnimFrames delay:0.2f];
    Anim.restoreOriginalFrame = NO;
    
    return [CCAnimate actionWithAnimation:Anim];
}

-(CCAnimate *)lookAroundAnima:(int) gender{
    NSString *fileType = (gender == 0) ? @"male_": @"female_";
    NSMutableArray *eyeAnimFrames = [NSMutableArray array];
    [eyeAnimFrames addObject:
     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
      [NSString stringWithFormat:@"%@eye_%d.png", fileType, 3]]];
    
    [eyeAnimFrames addObject:
     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
      [NSString stringWithFormat:@"%@eye_%d.png",fileType, 4]]];
    
    
    [eyeAnimFrames addObject:
     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
      [NSString stringWithFormat:@"%@eye_%d.png", fileType,1]]];
    
    CCAnimation *Anim = [CCAnimation animationWithSpriteFrames:eyeAnimFrames delay:1.0f];
    Anim.restoreOriginalFrame = YES;
    
    return [CCAnimate actionWithAnimation:Anim];
}

@end
