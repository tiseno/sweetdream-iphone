//
//  SDCustomVavigationBar.m
//  SweetDream
//
//  Created by desmond on 13-3-4.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import "SDCustomVavigationBar.h"

@implementation SDCustomVavigationBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    UIImage *image = [UIImage imageNamed: @"bar_top"];
    [image drawInRect:CGRectMake(0, 0, self.frame.size.width, 44)];
    
    //for iOS5
    [self setBackgroundImage:[UIImage imageNamed: @"bar_top"] forBarMetrics:UIBarMetricsDefault];
}

- (CGSize)sizeThatFits:(CGSize)size {
    CGSize newSize = CGSizeMake(320,44);
    return newSize;
}


@end
