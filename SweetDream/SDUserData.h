//
//  SDUserData.h
//  SweetDream
//
//  Created by desmond on 13-3-4.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SDUserData:NSObject

@property (nonatomic) NSInteger gender;    // 1 = male, 0 = female
@property (nonatomic) float height;    //body height
@property (nonatomic) float weight;    //body weight
@property (nonatomic) NSInteger sleppingPosition;  //"0 = side, 1 = back, 2 = stomach" slepper
@property (nonatomic) NSInteger firmness;  //mattress firmness  0 = soft, 1 = medium, 2 = firm
@property (nonatomic) NSString *supportFactor;
@property (nonatomic) NSString *supportFactorValue;
@property (nonatomic) NSString *supportFactorNsleepPosture;
@property (nonatomic) NSString *recommendedPillow;
@property (nonatomic) float bmi;

+ (SDUserData *)sharedSDUserData;

@end
