//
//  ColorBarCode.h
//  SweetDream
//
//  Created by desmond on 13-4-2.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface ColorBarCode : NSObject

@property(nonatomic) ccColor3B green;
@property(nonatomic)  ccColor3B green2;
@property(nonatomic)  ccColor3B yellow;
@property(nonatomic)  ccColor3B yellow2;
@property(nonatomic)  ccColor3B orange;
@property(nonatomic)  ccColor3B orange2;
@property(nonatomic)  ccColor3B red;
@property(nonatomic)  ccColor3B red2;

+(ColorBarCode *)sharedColorBarCode;
@end
