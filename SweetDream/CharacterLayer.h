//
//  CharacterLayer.h
//  SweetDream
//
//  Created by desmond on 13-3-15.
//  Copyright 2013年 desmond. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#include <stdlib.h>
#include "SDCustomAnimaLib.h"

@interface CharacterLayer : CCLayerColor {

}

typedef enum {
    LEFT_EYEBROWM = 0,
    RIGHT_EYEBROWM = 1,
    EYE = 3,
    MOUTH = 4,
} HeadPart;

@property(retain,nonatomic) CCLayerColor *bodyLayer;
@property(retain,nonatomic) CCLayerColor *eyeLayer;
@property(retain,nonatomic) CCLayerColor *hairLayer;
@property(retain,nonatomic) CCLayerColor *headLayer;

@property(retain,nonatomic) CCSprite *body;
@property(retain,nonatomic) CCSprite *mouth;
@property(retain,nonatomic) CCSprite *eye;

@property(retain,nonatomic) CCAnimation *mouthAnim1;
@property(retain,nonatomic) CCAnimation *mouthAnim2;
@property(retain,nonatomic) CCAnimation *mouthAnim3;

@property(nonatomic) BOOL characterDoPoping;

@property(nonatomic) int gender;

-(id)init;
-(void)setCharacterBody:(NSString *)url;
-(void)setCharactergender:(NSInteger) gender;
-(void)setCharacterPoping:(BOOL) setPoping;
@end
