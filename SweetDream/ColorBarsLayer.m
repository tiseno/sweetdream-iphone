//
//  ColorBarsLayer.m
//  SweetDream
//
//  Created by desmond on 13-4-2.
//  Copyright 2013年 desmond. All rights reserved.
//

#import "ColorBarsLayer.h"


@implementation ColorBarsLayer

-(id)init{
    self = [super init];
    if(self){
        
        return self;
    }
    return nil;
}

-(id)initWithArrayData:(NSArray*) data{
    self = [super initWithColor:ccc4(123, 123, 123, 0) width:[data count]*15 height:100];
    if(self){
        int i = 0;
        for(NSNumber *value in data){
            ColorBar *gra  = [[ColorBar alloc]initWithBarValue:[value integerValue] AndMaxValue:40 isRaiseUp:NO];
            gra.barWidth = 12;
            [gra setPosition:ccp(i*17,0)];
            [self addChild:gra];
            i ++;
        }
        return self;
    }
    return nil;
}
@end
