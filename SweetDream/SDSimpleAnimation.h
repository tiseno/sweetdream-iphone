//
//  SDSimpleAnimation.h
//  SweetDream
//
//  Created by desmond on 13-3-7.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@protocol SimpleAnimationDelegate <NSObject>
@required

-(void)didEndAnimation:(NSInteger)delegateIdentified;

@end

@interface SDSimpleAnimation : NSObject
{
}

@property (retain, nonatomic) id<SimpleAnimationDelegate> animaDelegate;
@property (nonatomic) NSInteger delegateIdentified;

+(SDSimpleAnimation* ) getAnimationManager;

-(void)doAnimation:(UIView *) view startPoint:(CGPoint)start maxPoint:(CGPoint)max endPoint:(CGPoint)end AndDirectionIsUP:(BOOL) directionIsUP;

-(void)leftPointerAnimation:(UIView *) view startPoint:(CGPoint)start maxPoint:(CGPoint)max endPoint:(CGPoint)end AndDirectionIsRight:(BOOL) directionIsRight;

-(void)doZoomInOutAnimation:(UIView *) view startScalePoint:(CGPoint)start EndScalePoint:(CGPoint)end;

-(void)doSpinningAnimation:(UIView *) view Origin:(float) OriDegree Destination:(float)DesDegree inTime:(float)time popEffect:(BOOL) effect;

@end
