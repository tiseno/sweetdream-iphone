//
//  SDCustomNavigationItem.h
//  SweetDream
//
//  Created by desmond on 13-3-6.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDCustomNavigationItem : UINavigationItem

@property (strong,nonatomic) UIButton *rightButton;
@property (strong,nonatomic) UIButton *leftButton;

-(id)initWithoutLeftButton;

@end
