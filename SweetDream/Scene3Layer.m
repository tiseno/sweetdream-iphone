//
//  Scene3Layer.m
//  SweetDream
//
//  Created by desmond on 13-4-3.
//  Copyright 2013年 desmond. All rights reserved.
//

#import "Scene3Layer.h"


@implementation Scene3Layer
@synthesize charactor, sc_acs;

+(CCScene *) scene
{
    @autoreleasepool {
        // 'scene' is an autorelease object.
        CCScene *scene = [CCScene node];
        
        Scene3Layer *layer = [Scene3Layer node];
        
        [scene addChild:layer];
        
        return scene;
    }
}

-(id)init{
    @autoreleasepool {
        if((self = [super init])) {
            [self initBackground];
            [self initCharactor];
            [self schedule:@selector(nextFrame:) interval:0 repeat:0 delay:0];
            
        }
        return self;
    }
}

-(void)initCharactor{
    NSString *gender = ([SDUserData sharedSDUserData].gender == 0)? @"": @"f_";
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:[NSString stringWithFormat:
                                                                          @"%@head.plist",gender]];
    
    CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode
                                      batchNodeWithFile:[NSString stringWithFormat:
                                                         @"%@head.png",gender]];
    
    NSMutableArray *walkAnimFrames = [NSMutableArray array];
    for(int i = 1; i <= 2; ++i) {
        [walkAnimFrames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"head_%d.png", i]]];
    }
    
    CCAnimation *walkAnim = [CCAnimation animationWithSpriteFrames:walkAnimFrames delay:0.2f];
    walkAnim.restoreOriginalFrame = NO;
    
    CGSize winSize = [CCDirector sharedDirector].winSize;
    charactor = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:
                                                     @"head_1.png",gender]];
    [charactor setAnchorPoint:ccp(0.5,0.5)];
    charactor.position = ccp(winSize.width / 2 , (winSize.height/2) - 50);
    
    CCAnimate *headAction = [CCAnimate actionWithAnimation:walkAnim];
    id anima0 = [CCMoveBy actionWithDuration:1 position:CGPointMake(0, 10)];
    id doneAction = [CCCallFuncN actionWithTarget:self selector:@selector(nextScene)];
    sc_acs = [CCSequence actions:anima0, headAction, doneAction,nil];
    
    [spriteSheet addChild:charactor];
    
    [self addChild:spriteSheet z:1];
}

-(void)initBackground{
    CGSize winSize = [CCDirector sharedDirector].winSize;
    CCSprite *background = [CCSprite spriteWithFile:@"bg_scene_3.png"];
    [background setAnchorPoint:ccp(0.5,0.5)];
    [background setPosition:ccp(winSize.width /2,winSize.height/2)];
    [self addChild:background z:0];
    
}

- (void) nextFrame:(ccTime)dt {
    [charactor runAction:sc_acs];
}

-(void)nextScene{
    CCTransitionFade *fade=[CCTransitionFade transitionWithDuration:3.0 scene:[Scene4Layer scene] withColor:ccBLACK];
    [[CCDirector sharedDirector]replaceScene:fade];

}

@end
