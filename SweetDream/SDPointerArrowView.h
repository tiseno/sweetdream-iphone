//
//  SDPointerArrowView.h
//  SweetDream
//
//  Created by desmond on 13-3-22.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDSimpleAnimation.h"

@interface SDPointerArrowView : UIView

@property (retain, nonatomic) UIImageView *arrowRight;
@property (retain, nonatomic) UIImageView *arrowLeft;

-(void)hideLeftArrow:(BOOL) flag;
-(void)hideRightArrow:(BOOL) flag;

@end
