//
//  SDInputHeightViewController.h
//  SweetDream
//
//  Created by desmond on 13-3-6.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCustomNavigationItem.h"
#import "SDPickerScrollView.h"
#import "QuartzCore/CAAnimation.h"
#import "SDUserData.h"
#import "ClockView.h"
#import "CharacterLayer.h"
#import "GenderPickerLayer.h"
#import "cocos2d.h"
#import "SDSimpleAnimation.h"

@interface SDInputHeightViewController : UIViewController<UITextFieldDelegate,UIPickerViewDelegate, UIPickerViewDataSource, UIActionSheetDelegate,UIScrollViewDelegate,UIAlertViewDelegate>{

}

@property (strong, nonatomic) IBOutlet UILabel *hintLabel;
@property (strong, nonatomic) IBOutlet SDPickerScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *clockArea;
@property (strong, nonatomic) IBOutlet SDCustomNavigationItem *navItem;
@property (retain, nonatomic) GenderPickerLayer *genderLayer;
@property (nonatomic) NSInteger height;

@property (strong, nonatomic)  UIPickerView  *picker;
@property (strong, nonatomic)  UIActionSheet *actionSheet;

//Picker component
@property (nonatomic) NSInteger ones;
@property (nonatomic) NSInteger tens;
@property (nonatomic) NSInteger hundreds;

// bottom title
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIImageView *title_background;

@end
