//
//  SDPickerScrollView.m
//  SweetDream
//
//  Created by desmond on 13-3-5.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import "SDPickerScrollView.h"

@implementation SDPickerScrollView
@synthesize tapDelegate;
-(id)initWithImageArray:(NSArray *) arrys AndScrollView:(UIScrollView *)scrollView AndItemFrame:(CGRect) ItemFrame{
    self = scrollView;
    if (self) {
        for (int i = 0; i < arrys.count; i++) {
            CGRect frame;
            frame.origin.x = self.frame.size.width * i;
            frame.origin.y = 0;
            frame.size = self.frame.size;
            
            UIImageView *character = [arrys objectAtIndex:i];
            character.tag = i;
            character.userInteractionEnabled = YES;
            character.frame = ItemFrame;
            
            UIView *subview = [[UIView alloc] initWithFrame:frame];
            subview.backgroundColor = [UIColor clearColor];
            
            [subview addSubview:character];
            [self addSubview:subview];
        }
        self.contentSize = CGSizeMake(self.frame.size.width * arrys.count, self.frame.size.height);
    }
    return self;
}

//not working !!!
- (void)findOutTheTag:(id)sender {
    [self.tapDelegate findOutTheTag:sender];
}

-(void)scrollToPage:(NSInteger) pageNumber{
    pageNumber = pageNumber <= -1?0:pageNumber;
    CGPoint scrollVIew_offset = CGPointMake(pageNumber*320, 0);
    [self setContentOffset:scrollVIew_offset animated:YES ];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

@end
