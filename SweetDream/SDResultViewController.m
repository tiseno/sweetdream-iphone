//
//  SDResultViewController.m
//  SweetDream
//
//  Created by desmond on 13-3-7.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import "SDResultViewController.h"
//#import "Facebook.h"

@interface SDResultViewController ()

@end

@implementation SDResultViewController
@synthesize resultView,slideUPLabel,supportFactorLabel,navItem,resultArrow,recommendedPillow;
@synthesize resultContentView,slideUpTitleLabel,viewPages,orOboveLabel,resultDropdownView;
@synthesize supportFactorResultLabel,supportFactorResultLabel2,descLabel;
@synthesize pageindex,pageNumber,indexButton;

BOOL isShowingResultView;
BOOL isShowPillowView;
NMCustomLabel *label1;

#define SYSTEM_VERSION_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navItem initWithoutLeftButton];
    self.navItem.hidesBackButton = YES;
    [self.navItem.rightButton addTarget:self action:@selector(backToHome:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.navItem.leftButton addTarget:self action:@selector(showRecommendedPillowView) forControlEvents:UIControlEventTouchUpInside];
    
    [slideUPLabel setFont:[UIFont fontWithName:@"CreteRound-Regular" size:slideUPLabel.font.pointSize]];
//    [supportFactorLabel setFont:[UIFont fontWithName:@"CreteRound-Regular" size:supportFactorLabel.font.pointSize]];
    [orOboveLabel setFont:[UIFont fontWithName:@"CreteRound-Regular" size:orOboveLabel.font.pointSize]];
    [slideUpTitleLabel setFont:[UIFont fontWithName:@"CreteRound-Regular" size:orOboveLabel.font.pointSize]];
    [supportFactorResultLabel setFont:[UIFont fontWithName:@"CreteRound-Regular" size:supportFactorResultLabel.font.pointSize]];
    [supportFactorResultLabel2 setFont:[UIFont fontWithName:@"CreteRound-Regular" size:supportFactorResultLabel2.font.pointSize]];
    [descLabel setFont:[UIFont fontWithName:@"CreteRound-Regular" size:descLabel.font.pointSize]];
        
    [NSTimer scheduledTimerWithTimeInterval: 4
                target: self
              selector: @selector(doAnimation)
              userInfo: nil
               repeats: YES];
    
    
    viewPages = [[NSMutableArray alloc]init];
    [self initData];
    [self initFAQ];
}

-(void)viewDidDisappear:(BOOL)animated{
    [self setViewPages:nil];
    for (UIView *v in viewPages) {
        [v removeFromSuperview];
    }
}

-(void)initFAQ{
    //init result view position
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat originalPos = screenRect.size.height - 127.0;
    
    CGRect viewFrame = CGRectMake(5, originalPos, resultView.frame.size.width, resultView.frame.size.height);
    resultView.frame = viewFrame;
    //
    
    pageindex = 0;
    //get faq & faq title from plist
    NSBundle *bundle = [NSBundle mainBundle];
    NSURL *plistURL = [bundle URLForResource:@"faq" withExtension:@"plist"];
    NSURL *FaqTitleURL = [bundle URLForResource:@"faq_title" withExtension:@"plist"];
    
    NSArray *faqs = [NSArray arrayWithContentsOfURL:plistURL];
    NSArray *FaqTitles = [NSArray arrayWithContentsOfURL:FaqTitleURL];
    //
    
    //init faq index page
    SDFAQTable *faqIndex = [[SDFAQTable alloc]initWithFrame:CGRectMake(15, 0, 270, resultContentView.frame.size.height) AndDataArray:FaqTitles];
    faqIndex.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
    faqIndex.FAQdelegate = self;
    
    [self.resultContentView addSubview:faqIndex];
    [viewPages addObject:faqIndex];
    //
    
    //init faq content pages
    for(int i = 0 ; i < [faqs count] ; i++){
        UIView *v = [[UIView alloc]initWithFrame:CGRectMake(15+((i + 1)* 320), 0, 280, resultContentView.frame.size.height - 45)];
        [v setBackgroundColor:[UIColor whiteColor]];
        v.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
        UIScrollView *scroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, 280, resultContentView.frame.size.height - 45)];
        [scroll setBackgroundColor:[UIColor whiteColor]];
        NMCustomLabel *label = [[NMCustomLabel alloc]initWithFrame:CGRectMake(0, 5, 270, 640)];
        
        
        label.text = [faqs objectAtIndex:i];
        [label setDefaultStyle:[NMCustomLabelStyle styleWithFont:[UIFont fontWithName:@"CreteRound-Regular" size:13] color:[UIColor colorWithRed:64/256.0 green:64/256.0 blue:64/256.0 alpha:1.0]]];
        [label setStyle:[NMCustomLabelStyle styleWithFont:[UIFont fontWithName:@"CreteRound-Regular" size:13] color:[UIColor colorWithRed:64/256.0 green:64/256.0 blue:64/256.0 alpha:1.0]] forKey:@"bold"];
        [label setStyle:[NMCustomLabelStyle styleWithFont:[UIFont fontWithName:@"CreteRound-Regular" size:15] color:[UIColor colorWithRed:140/256.0 green:73/256.0 blue:0/256.0 alpha:1.0]] forKey:@"header"];
        
        label.numberOfLines = 0;
        [label sizeToFit];
        
        [scroll setContentSize:CGSizeMake(label.frame.size.width, label.frame.size.height + 15) ];
        [scroll addSubview:label];
        [v addSubview:scroll];
        
        [self.resultContentView addSubview:v];
        [viewPages addObject:v];
    }
    //
    
    //set swip gesture to faq area
    @autoreleasepool {
        UISwipeGestureRecognizer *swipLeftReconizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipPage:)];
        [swipLeftReconizer setDirection: (UISwipeGestureRecognizerDirectionLeft)];
        
        UISwipeGestureRecognizer *swipRightReconizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipPage:)];
        [swipRightReconizer setDirection: (UISwipeGestureRecognizerDirectionRight)];
        [self.resultContentView  addGestureRecognizer:swipLeftReconizer];
        [self.resultContentView  addGestureRecognizer:swipRightReconizer];
    }
    //
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    isShowingResultView = false;
    isShowPillowView = NO;
    self.navigationController.navigationBarHidden = NO;
    
    //drop down animation
    [[SDSimpleAnimation getAnimationManager] doAnimation:resultDropdownView startPoint:CGPointMake(7, -175) maxPoint:CGPointMake(7, 0) endPoint:CGPointMake(7, -11) AndDirectionIsUP:NO];

    }

-(void)showRecommendedPillowView{
    if(!isShowPillowView){
        CGRect panelFrame = CGRectMake(0, -10, self.view.bounds.size.width, self.view.bounds.size.height + 10);
        RecommendedPillowView *modalPanel = [[RecommendedPillowView alloc]initWithFrame:panelFrame title:@""];
        modalPanel.label.text = [SDUserData sharedSDUserData].recommendedPillow;
        modalPanel.delegate = self;
        [self.view addSubview:modalPanel];
        [modalPanel showFromPoint:CGPointMake(160, 240)];
        isShowPillowView = YES;
    }
}

//play button spinning animation
-(void)doAnimation{
    [[SDSimpleAnimation getAnimationManager] doSpinningAnimation:self.playButton Origin:0 Destination:360 inTime:1 popEffect:YES];
}

//set data to label
-(void)initData{
    supportFactorResultLabel.text = [SDUserData sharedSDUserData].supportFactorNsleepPosture;
    supportFactorLabel.text = [SDUserData sharedSDUserData].supportFactorValue;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)backToHome:(id)sender{
    [[CCDirector sharedDirector] end];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

//show/hide faq 
- (IBAction)showHideResult:(id)sender {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat originalPos = screenRect.size.height - 127.0;
    
    if(!isShowingResultView){
        [[SDSimpleAnimation getAnimationManager] doAnimation:resultView startPoint:CGPointMake(5, originalPos) maxPoint:CGPointMake(5, -30) endPoint:CGPointMake(5, -10) AndDirectionIsUP:YES];
        [[SDSimpleAnimation getAnimationManager] doSpinningAnimation:self.resultArrow Origin:0 Destination:180 inTime:0.5 popEffect:NO];
        isShowingResultView = true;
    }else{
        [[SDSimpleAnimation getAnimationManager] doAnimation:resultView startPoint:CGPointMake(5, 20) maxPoint:CGPointMake(5, originalPos) endPoint:CGPointMake(5, originalPos) AndDirectionIsUP:NO];
        [[SDSimpleAnimation getAnimationManager] doSpinningAnimation:self.resultArrow Origin:90 Destination:0 inTime:0.5 popEffect:NO];
        isShowingResultView = false;
    }
}

//play animation
- (IBAction)playVideo:(id)sender {
    [[SDSimpleAnimation getAnimationManager] doAnimation:resultDropdownView startPoint:CGPointMake(7, -11) maxPoint:CGPointMake(7, -400) endPoint:CGPointMake(7, -404) AndDirectionIsUP:YES];
    
    [NSTimer scheduledTimerWithTimeInterval:.5 target:self selector:@selector(popView) userInfo:nil repeats:NO];
}

-(void)popView{
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)goSweetDreamWebsite:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://sweetdream.com.my/"]];
}

#pragma mark-
#pragma mark- SwipGesture delegate
//when swipping the faq page
- (IBAction)swipPage:(UISwipeGestureRecognizer *)recognizer{
    if (recognizer.direction == UISwipeGestureRecognizerDirectionRight) {
        [self flipingNext:NO numberOfPages:1];
    }
    
    if (recognizer.direction == UISwipeGestureRecognizerDirectionLeft) {
        [self flipingNext:YES numberOfPages:1];
    }
}


//click next/previous page button
- (IBAction)flipPage:(id)sender {
    if([sender tag] == 99){
        [self flipingNext:NO numberOfPages:1];
    }else if([sender tag] == 100){
        [self flipingNext:YES numberOfPages:1];
    }
}

-(void)flipingNext:(BOOL)flipNext numberOfPages:(NSInteger)number{
    [SDAppDelegate playSoundEffect:2];
    [indexButton setEnabled:YES];
    UIButton *button;
    NSInteger prevPage = pageindex;
    
    //flip prevoues
    if(!flipNext && pageindex > 0){
        pageindex -= number;
        
        button = (UIButton*)[self.view viewWithTag:100];
        [button setEnabled:YES];
        
        UIView *v1 = [viewPages objectAtIndex:prevPage];
        UIView *v2 = [viewPages objectAtIndex:pageindex];

        [MPFlipTransition transitionFromView:v1
                                      toView:v2
                                    duration:[MPTransition defaultDuration]
                                       style:NO? MPFlipStyleDefault	: MPFlipStyleFlipDirectionBit( MPFlipStyleDefault)
                            transitionAction:MPTransitionActionAddRemove
                                  completion:nil];
        
    }else if(flipNext && pageindex < [viewPages count] - 1){        //flip next
        pageindex += number;
        
        button = (UIButton*)[self.view viewWithTag:99];
        [button setEnabled:YES];
        
        UIView *v1 = [viewPages objectAtIndex:prevPage];
        UIView *v2 = [viewPages objectAtIndex:pageindex];
        
        [MPFlipTransition transitionFromView:v1
                                      toView:v2
                                    duration:[MPTransition defaultDuration]
                                       style:YES? MPFlipStyleDefault: MPFlipStyleFlipDirectionBit( MPFlipStyleDefault)
                            transitionAction:MPTransitionActionAddRemove
                                  completion:nil];
    }
    
    slideUpTitleLabel.text = [NSString stringWithFormat:@"FAQ %d/13",pageindex];
    
    if (pageindex == 0) {
        button = (UIButton *)[self.view viewWithTag:99];
        [button setEnabled:NO];
        [indexButton setEnabled:NO];
        slideUpTitleLabel.text = [NSString stringWithFormat:@"Index"];
    }else if(pageindex == 13){
        button = (UIButton *)[self.view viewWithTag:100];
        [button setEnabled:NO];
        [button setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    }
}

- (void)didCloseModalPanel:(UAModalPanel *)modalPanel{
    isShowPillowView = NO;
}

- (IBAction)shareLink:(id)sender {
    UIActionSheet *actionsheet = [[UIActionSheet alloc] initWithTitle:@""
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Facebook", @"Email", nil];
    [actionsheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    [actionsheet showInView:self.view];
    }

// ios5 share actionsheet events
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == actionSheet.firstOtherButtonIndex + 0) {
        [self share2Facebook];
    } else if (buttonIndex == actionSheet.firstOtherButtonIndex + 1) {
        // email is clicked
        if ([MFMailComposeViewController canSendMail]) {
            MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
            [mail setMessageBody:@"Check out your mattress number with Sweetdream App - My Mattress Number. Click the link at https://itunes.apple.com/us/app/sweetdream-my-mattress-number/id627904788?mt=8&ign-mpt=uo%3D4" isHTML:NO];
            [mail setMailComposeDelegate:self];
            [self presentModalViewController:mail animated:YES];
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email"
                                                            message:@"No email app detected"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
    }
}

// share events
-(void)share2Facebook {
    //ios above 6.0
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0"))
    {
        BOOL displayedNativeDialog = [FBNativeDialogs presentShareDialogModallyFrom:self
                                                                        initialText:@"Check out your mattress number with Sweetdream App – My Mattress Number for Iphone. "
                                                                              image:nil
                                                                                url:[NSURL URLWithString:@"https://itunes.apple.com/us/app/sweetdream-my-mattress-number/id627904788?mt=8&ign-mpt=uo%3D4"]
                                                                            handler:nil];
        if (!displayedNativeDialog) {
            [self postStatus];
        }
    }
    
    if (SYSTEM_VERSION_LESS_THAN(@"6.0"))
    {
        SDAppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
        
        if (appDelegate.session.isOpen) {
            [self postStatus];
            
        } else {
            NSLog(@"appDelegate.session.state %d",appDelegate.session.state);
            if (appDelegate.session.state == FBSessionStateCreated) {
                // Create a new, logged out session.
                appDelegate.session = [[FBSession alloc] init];
            }
            
            [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                             FBSessionState status,
                                                             NSError *error) {
                [self postStatus];
            }];
        }
    }
    
}

-(void)postStatus{
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"http://itunes.apple.com/us/app/sweetdream-my-mattress-number/id627904788?mt=8&ign-mpt=uo%3D4", @"link",
                                   @"SweetDream", @"name",
                                   @"Mattress Number", @"caption",
                                   @"Check out your mattress number with Sweetdream App – My Mattress Number for Iphone.", @"description",
                                   nil];
    SDAppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    [FBWebDialogs presentFeedDialogModallyWithSession:appDelegate.session parameters:params handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
        NSLog(@"Error : %@",error);
        if (error) {
            NSLog(@"Error publishing story.");
        } else {
            if (result == FBWebDialogResultDialogNotCompleted) {
                NSLog(@"User canceled story publishing.");
            } else {
                NSLog(@"Posted story");
            }
        }
    }];
}

#pragma mark-
#pragma mark- MFMailComposeViewController Delegate
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [self dismissModalViewControllerAnimated:YES];
}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    [self dismissModalViewControllerAnimated:YES];
}

#pragma FAQTable Delegate
-(void)didSelected:(NSIndexPath *)indexPath{
    [self flipingNext:YES numberOfPages:indexPath.row + 1];
}

- (void)viewDidUnload {
    [self setResultArrow:nil];
    [self setPlayButton:nil];
    [self setPlayButton:nil];
    [self setResultContentView:nil];
    [self setNavItem:nil];
    [self setResultView:nil];
    [self setSlideUpTitleLabel:nil];
    [self setRecommendedPillow:nil];
    [self setSupportFactorLabel:nil];
    [self setOrOboveLabel:nil];
    [self setResultDropdownView:nil];
    [self setSupportFactorResultLabel:nil];
    [self setDescLabel:nil];
    [self setSupportFactorResultLabel2:nil];
    [self setIndexButton:nil];
    [super viewDidUnload];
}

- (IBAction)backToIndex:(id)sender {
    [self flipingNext:NO numberOfPages:pageindex];
}
@end
