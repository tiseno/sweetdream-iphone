//
//  SoundEffect.h
//  SweetDream
//
//  Created by desmond on 13-3-27.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioServices.h>

@interface SoundEffect : NSObject
{
    SystemSoundID soundID;
    SystemSoundID soundID2;
}

- (id)initWithSoundNamed:(NSString *)filename;
- (void)play;
- (void)playTick;

@end