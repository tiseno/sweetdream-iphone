//
//  ColorBar.m
//  SweetDream
//
//  Created by desmond on 13-4-1.
//  Copyright 2013年 desmond. All rights reserved.
//

#import "ColorBar.h"

@implementation ColorBar
@synthesize value, maxValue,currentValue,hitValue;
@synthesize increseDirection,color;
@synthesize barWidth ,randomIncrese;

-(id)initWithBarValue:(NSInteger)value AndMaxValue:(NSInteger)maxValue isRaiseUp:(BOOL)flag{
    self = [super initWithColor:ccc4(255, 255, 109, 255) fadingTo:ccc4(172, 215, 0, 255)];
    if(self){

        self.currentValue = 0;
        self.hitValue = NO;
        self.randomIncrese = YES;
        self.increseDirection = flag? 0 : 1;
        self.value = value;
        self.maxValue = maxValue;
        self.barWidth = 10; //default
        [self setAnchorPoint:ccp(0.5,0.5)];
        [self setContentSize:CGSizeMake(barWidth, self.currentValue)];
        [self schedule:@selector(increseBar) interval:1/60 repeat:-1 delay:3];
        return self;
    }
    return nil;
}

-(void)increseBar{

    [self setRotation:180 * self.increseDirection];
    if(!hitValue && !randomIncrese){
        [self setContentSize:CGSizeMake(barWidth, currentValue)];
        [self setPosition:CGPointMake(self.boundingBox.origin.x, self.boundingBox.origin.y - (2 * self.increseDirection))];
        self.currentValue = self.currentValue+2;
        
        
        
        hitValue = (self.currentValue >= value);
    }else if(randomIncrese){    //random increse 
        int increse = (rand() % 2) + 1;
        if(!hitValue){
            [self setContentSize:CGSizeMake(barWidth, currentValue)];
            [self setPosition:CGPointMake(self.boundingBox.origin.x, self.boundingBox.origin.y - (increse * self.increseDirection))];
            self.currentValue = self.currentValue + increse;
            hitValue = (self.currentValue >= maxValue);
        }else{
            [self setContentSize:CGSizeMake(barWidth, currentValue)];
            [self setPosition:CGPointMake(self.boundingBox.origin.x, self.boundingBox.origin.y + (increse * self.increseDirection))];
            self.currentValue = self.currentValue - increse;
            randomIncrese = !(self.currentValue <= 0);
            currentValue = randomIncrese ? currentValue : 0;
            hitValue = randomIncrese;   // when current value become 0 then switch to actual value increse
        }
    }
    
    if (currentValue <= maxValue*.25) {
        [self setStartColor:[ColorBarCode sharedColorBarCode].orange];
        [self setEndColor:[ColorBarCode sharedColorBarCode].orange2];
    }else if(currentValue >= maxValue*.25 && currentValue <= maxValue*.50){
        [self setStartColor:[ColorBarCode sharedColorBarCode].orange];
        [self setEndColor:[ColorBarCode sharedColorBarCode].orange2];
    }else if(currentValue >= maxValue*.50 && currentValue <= maxValue*.75){
        [self setStartColor:[ColorBarCode sharedColorBarCode].red];
        [self setEndColor:[ColorBarCode sharedColorBarCode].red2];
    }else if(currentValue >= maxValue){
        [self setStartColor:[ColorBarCode sharedColorBarCode].red];
        [self setEndColor:[ColorBarCode sharedColorBarCode].red2];
    }
}
@end
