//
//  SDFirmnessViewController.h
//  SweetDream
//
//  Created by desmond on 13-3-6.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCustomNavigationItem.h"
#import "SDPickerScrollView.h"
#import "QuartzCore/CAAnimation.h"
#import "SDSimpleAnimation.h"
#import "SDUserData.h"

@interface SDFirmnessViewController : UIViewController<SimpleAnimationDelegate>
@property (strong, nonatomic) IBOutlet SDPickerScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UILabel *hintLabel;
@property (strong, nonatomic) IBOutlet SDCustomNavigationItem *navItem;


@end
