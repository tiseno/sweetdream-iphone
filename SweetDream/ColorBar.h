//
//  ColorBar.h
//  SweetDream
//
//  Created by desmond on 13-4-1.
//  Copyright 2013年 desmond. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "ColorBarCode.h"
#include <stdlib.h>

@interface ColorBar : CCLayerGradient {
    
}
@property(nonatomic) NSInteger value;   // target value
@property(nonatomic) NSInteger maxValue; //100% value
@property(nonatomic) NSInteger currentValue;
@property(nonatomic) NSInteger increseDirection;
@property(nonatomic) BOOL hitValue;
@property(nonatomic) BOOL  randomIncrese;

@property(nonatomic) NSInteger barWidth;

-(id)initWithBarValue:(NSInteger)value AndMaxValue:(NSInteger)maxValue isRaiseUp:(BOOL)flag;

@end
