//
//  SDFAQTable.m
//  SweetDream
//
//  Created by desmond on 13-4-25.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import "SDFAQTable.h"

@implementation SDFAQTable
@synthesize data,FAQdelegate;

- (id)initWithFrame:(CGRect)frame AndDataArray:(NSArray *)data
{
    self = [super initWithFrame:frame];
    if (self) {
        self.separatorColor = [UIColor colorWithRed:140.0/255.0 green:73.0/255.0 blue:0.0/255.0 alpha:1];
        self.data = data;
        self.delegate = self;
        self.dataSource = self;
    }
    return self;
}

#pragma tableView Data Source delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [data count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    static NSString *CellIdentifier = @"cellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    cell.textLabel.text = [data objectAtIndex:indexPath.row];

    [cell.textLabel setFont:[UIFont fontWithName:@"CreteRound-Regular" size:14]];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    [cell.textLabel sizeToFit];
    cell.textLabel.textColor = [UIColor colorWithRed:140.0/255.0 green:73.0/255.0 blue:0.0/255.0 alpha:1];
    cell.textLabel.numberOfLines = 0;

    return cell;
    
}

#pragma tableView Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [FAQdelegate didSelected:indexPath];
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellText = [self.data objectAtIndex:indexPath.row];
    UIFont *cellFont = [UIFont fontWithName:@"CreteRound-Regular" size:17.0];
    CGSize constraintSize = CGSizeMake(280.0f, MAXFLOAT);
    CGSize labelSize = [cellText sizeWithFont:cellFont constrainedToSize:constraintSize lineBreakMode:UILineBreakModeWordWrap];
    
    return labelSize.height;
}
@end
