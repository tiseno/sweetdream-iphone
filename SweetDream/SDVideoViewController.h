//
//  SDVideoViewController.h
//  SweetDream
//
//  Created by desmond on 13-3-26.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"
#import "SDAppDelegate.h"
#import "Scene1Layer.h"
#import "Scene3Layer.h"

@interface SDVideoViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *nextButton;
@property (strong, nonatomic) IBOutlet UIButton *repeatButton;
@property (strong,nonatomic) NSTimer *timer;

- (IBAction)repeat:(id)sender;

@end
