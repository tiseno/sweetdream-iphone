//
//  SDCustomLayerDelegate.h
//  SweetDream
//
//  Created by desmond on 13-3-20.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CustomLayerDelegate <NSObject>
@required
-(void)ItemDidSelect:(NSInteger )result;
@end

@interface SDCustomLayerDelegate : NSObject{
    
}

@property (retain, nonatomic) id<CustomLayerDelegate> layerDelegate;

-(id<CustomLayerDelegate>) getCustomLayerDelegate;
+(SDCustomLayerDelegate* ) getCustomLayerManager;
@end
