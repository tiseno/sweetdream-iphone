//
//  SDGenderPickerViewController.h
//  SweetDream
//
//  Created by desmond on 13-3-4.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDAppDelegate.h"
#import "ClockView.h"
#import "SDPickerScrollView.h"
#import "SDUserData.h"
#import "SDSimpleAnimation.h"
#import "SDCustomNavigationItem.h"
#import "cocos2d.h"
#import "GenderPickerLayer.h"
#import "CharacterLayer.h"
#import "ClockView.h"
#import "SDPointerArrowView.h"
#import "SoundEffect.h"

@interface SDGenderPickerViewController : UIViewController<SimpleAnimationDelegate,UIScrollViewDelegate,CustomLayerDelegate,UIGestureRecognizerDelegate,UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet SDCustomNavigationItem *navItem;
@property (strong, nonatomic) IBOutlet UIView *clockArea;
@property (retain, nonatomic) IBOutlet SDPickerScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UILabel *hintLabel;
@property (strong, nonatomic) IBOutlet UIView *elementView;
@property (strong, nonatomic) IBOutlet UIView *rootUIView;
@property (strong, nonatomic) SDPointerArrowView *arrow;
@property (retain, nonatomic) GenderPickerLayer *genderPickLayer;

// bottom title
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIImageView *title_background;

- (IBAction)switchItem:(id)sender;

@end
