//
//  SDCustomNavigationItem.m
//  SweetDream
//
//  Created by desmond on 13-3-6.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import "SDCustomNavigationItem.h"

@implementation SDCustomNavigationItem
@synthesize leftButton, rightButton;

-(id)init{
    //custom back & next button
    self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.leftButton setImage:[UIImage imageNamed:@"btn_prev.png"] forState:UIControlStateNormal];
    self.leftButton.frame = CGRectMake(0, 0, 35, 35);
    
    self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.rightButton setImage:[UIImage imageNamed:@"btn_next.png"] forState:UIControlStateNormal];
    self.rightButton.frame = CGRectMake(0, 0, 35, 35);
    
    self.leftBarButtonItem= [[UIBarButtonItem alloc] initWithCustomView: leftButton] ;
    
    self.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: rightButton];
    
    return self;
}

-(id)initWithoutLeftButton{
    self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.leftButton setImage:[UIImage imageNamed:@"btn_pillow.png"] forState:UIControlStateNormal];
    self.leftButton.frame = CGRectMake(0, 0, 55, 35);
    
    self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.rightButton setImage:[UIImage imageNamed:@"btn_repeat.png"] forState:UIControlStateNormal];
    self.rightButton.frame = CGRectMake(-10, 0, 35, 35);
    
    self.leftBarButtonItem= [[UIBarButtonItem alloc] initWithCustomView: leftButton] ;
    self.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: rightButton];
    
    return self;
}


@end
