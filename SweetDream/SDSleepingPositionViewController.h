//
//  SDSleepingPositionViewController.h
//  SweetDream
//
//  Created by desmond on 13-3-5.
//  Copyright (c) 2013年 desmond. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDPickerScrollView.h"
#import "QuartzCore/CAAnimation.h"
#import "SDCustomNavigationItem.h"
#import "SDSimpleAnimation.h"
#import "SDUserData.h"
#import "SDPointerArrowView.h"
#import "SDAppDelegate.h"

@interface SDSleepingPositionViewController : UIViewController<SimpleAnimationDelegate>
@property (retain, nonatomic) IBOutlet SDPickerScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *hintLabel;
@property (strong, nonatomic) IBOutlet SDCustomNavigationItem *navItem;
@property (strong, nonatomic) SDPointerArrowView *arrow;

// bottom title
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIImageView *title_background;
- (IBAction)switchItem:(id)sender;

@end
